#pragma once

#include "stdafx.h"
#include "XeUtilitiesApp.h"
#include "XeUtilitiesUI.h"
#include "Tools\SceneManager\SceneManager.h"
#include "Scenes\ScnMain.h"
#include "Scenes\ScnMainParent.h"

HRESULT XeUtilitiesUI::InitializeUI( void * pInitData )
{
	HRESULT retVal = NULL;

	retVal = InitShared( XeUtilitiesApp::getInstance().m_pd3dDevice, &(XeUtilitiesApp::getInstance().m_d3dpp), /*XuiTextureLoader*/XuiD3DXTextureLoader );
	if( FAILED( retVal ) )
	{
		return retVal;
	}

	
	retVal = XuiRenderGetXuiDevice(GetDC(), &m_pXuiDevice);
	if(retVal != S_OK)
	{
		m_pXuiDevice = NULL;
	}

	// Register the default font and call it ConsoleFont
	retVal = RegisterDefaultTypeface( L"ConsoleFont", L"file://game:/Media/Fonts/xarialuni.ttf" );
	if( FAILED( retVal ) )
		return retVal;

	SceneManager::getInstance().SetSkin( *this, "Main.xur");

	return S_OK;
}

HRESULT XeUtilitiesUI::PreRenderUI( void *pRenderData )
{
	PreRender();
	return S_OK;
}

HRESULT XeUtilitiesUI::RenderUI( D3DDevice * pDevice, int nHOverscan, int nVOverscan, UINT uWidth, UINT uHeight)
{
	XuiTimersRun();
	XuiRenderBegin( GetDC(), D3DCOLOR_ARGB( 255, 0, 0, 0) );

	D3DXMATRIX matOrigView;
	D3DXMATRIX matOrigTrans;
	XuiRenderGetViewTransform( GetDC(), &matOrigView );

	// Scale depending on width and height
	D3DXMATRIX matView;
	int NewWidth = uWidth - (nHOverscan * 2);
	int NewHeight = uHeight - (nVOverscan * 2);
	D3DXVECTOR2 vScaling = D3DXVECTOR2( NewWidth / 1280.0f, NewHeight / 720.0f );
	D3DXVECTOR2 vTranslation = D3DXVECTOR2( (float)nHOverscan, (float)nVOverscan );
	D3DXMatrixTransformation2D( &matView, NULL, 0.0f, &vScaling, NULL, 0.0f, &vTranslation);
	XuiRenderSetViewTransform( GetDC(), &matView );

	XUIMessage msg;
	XUIMessageRender msgRender;
	XuiMessageRender( &msg, &msgRender, GetDC(), 0xffffffff, XUI_BLEND_NORMAL );
	XuiSendMessage( GetRootObj(), &msg );

	XuiRenderSetViewTransform( GetDC(), &matOrigView );
	XuiRenderEnd( GetDC() );

	return S_OK;
}

HRESULT XeUtilitiesUI::UpdateUI( void * pUpdateData )
{
	RunFrame();
	return S_OK;
}

void XeUtilitiesUI::RunFrame()
{
	CXuiModule::RunFrame();
}

void XeUtilitiesUI::DispatchXUIInput( XINPUT_KEYSTROKE* pKeystroke )
{
	XuiProcessInput(pKeystroke);
}


HRESULT XeUtilitiesUI::CreateMainCanvas()
{
	ASSERT( m_bXuiInited );
	if( !m_bXuiInited )
	{
		return E_UNEXPECTED;
	}

	ASSERT( m_hObjRoot == NULL );
	if( m_hObjRoot )
	{
		return E_UNEXPECTED;
	}

	HRESULT retVal = XuiCreateObject( L"XuiCanvas", &m_hObjRoot );
	if( FAILED( retVal ) ) 
	{ 
		return retVal; 
	}

	return retVal;
}

HRESULT XeUtilitiesUI::RegisterXuiClasses()
{
	ScnMainParent::Register();
	ScnMain::Register();
	return S_OK;
}

HRESULT XeUtilitiesUI::UnregisterXuiClasses()
{
	ScnMainParent::Unregister();
	ScnMain::Unregister();
	return S_OK;
}