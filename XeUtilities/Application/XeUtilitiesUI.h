#pragma once

class XeUtilitiesUI : public CXuiModule
{

private:
	IXuiDevice * m_pXuiDevice;

public:

	static WCHAR szSkinPath[ ATG::LOCATOR_SIZE ];

	static WCHAR szMainScenePath[ ATG::LOCATOR_SIZE ];

	XeUtilitiesUI() {}
	~XeUtilitiesUI() {}

	XeUtilitiesUI(const XeUtilitiesUI&);
	XeUtilitiesUI& operator=(const XeUtilitiesUI&);

	static XeUtilitiesUI& getInstance()
	{
		static XeUtilitiesUI singleton;
		return singleton;
	}

	IXuiDevice * getXuiDevice() { return m_pXuiDevice; }

	virtual void RunFrame();
	void DispatchXUIInput( XINPUT_KEYSTROKE* pKeystroke );
	HRESULT InitializeUI( void * pInitData );
	HRESULT PreRenderUI( void * pRenderData );
	HRESULT RenderUI( D3DDevice * pDevice, int nHOverscan, int nVOverscan, UINT uWidth, UINT uHeight);
	HRESULT UpdateUI( void * pUpdateData );

	HRESULT CreateMainCanvas();
	HRESULT RegisterXuiClasses();
	HRESULT UnregisterXuiClasses();
};