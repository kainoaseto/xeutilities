#pragma once

#include "stdafx.h"
#include "XeUtilitiesApp.h"
#include "XeUtilitiesUI.h"
#include "Tools\Settings\Settings.h"
#include "Tools\Xam\HvPeekPoke.h"

HRESULT XuiTextureLoader(IXuiDevice *pDevice, LPCWSTR szFileName, XUIImageInfo *pImageInfo, IDirect3DTexture9 **ppTex)
{
	int Len = wcslen(szFileName);
	if(szFileName[Len-3] == L'P' || szFileName[Len-3] == L'p')
	{
		return XuiPNGTextureLoader(pDevice, szFileName, pImageInfo, ppTex);
	}
	return XuiD3DXTextureLoader(pDevice, szFileName, pImageInfo, ppTex);
}

XeUtilitiesApp::XeUtilitiesApp( void )
{
	HRESULT hr = InitializeHvPeekPoke();
	if(InitializeHvPeekPoke() != ERROR_SUCCESS)
	{
		printf("InitializeHvPeekPoke Failed %x\n", hr);
	}
}


HRESULT XeUtilitiesApp::CreateRenderTargets( void )
{
	HRESULT retVal;

	//Initialize D3DSurface Parameters
	D3DSURFACE_PARAMETERS ddsParams = { 0 };

	//Create RenderTargets for use with Predicted Tiling
	retVal = m_pd3dDevice->CreateRenderTarget( g_dwTileWidth, g_dwTileHeight, D3DFMT_X8R8G8B8, D3DMULTISAMPLE_4_SAMPLES, 0, 0, &m_pBackBuffer, &ddsParams );
	if( retVal != D3D_OK)
	{
		printf("Predicted Tiling Render Target failed to create");
		return S_FALSE;
	}

	// Set up the surface size parameters
	ddsParams.Base = m_pBackBuffer->Size / GPU_EDRAM_TILE_SIZE;
	ddsParams.HierarchicalZBase = 0;

	//Create Depth Stencil Surface
	retVal = m_pd3dDevice->CreateDepthStencilSurface( g_dwTileWidth, g_dwTileHeight, D3DFMT_D24S8, D3DMULTISAMPLE_4_SAMPLES, 0, 0, &m_pDepthBuffer, &ddsParams );
	if(retVal != D3D_OK)
	{
		printf("Stencil surface failed to create");
		return S_FALSE;
	}

	//Create First Frame buffer
	retVal = m_pd3dDevice->CreateTexture( g_dwFrameWidth, g_dwFrameHeight, 1, 0, D3DFMT_LE_X8R8G8B8, 0, &m_pFrontBuffer[0], NULL );
	if(retVal != D3D_OK)
	{
		printf("Failed to create frame buffer #1");
		return S_FALSE;
	}

	// Create Second Frame Buffer
	retVal = m_pd3dDevice ->CreateTexture( g_dwFrameWidth, g_dwFrameHeight, 1, 0, D3DFMT_LE_X8R8G8B8, 0, &m_pFrontBuffer[1], NULL );
	if(retVal != D3D_OK)
	{
		printf("Failed to create frame buffer #2");
		return S_FALSE;
	}

	return S_OK;

}

HRESULT XeUtilitiesApp::Initialize()
{
	CreateRenderTargets();

	XeUtilitiesUI::getInstance().InitializeUI(NULL);

	return S_OK;
}

HRESULT XeUtilitiesApp::Render()
{
	XeUtilitiesUI::getInstance().PreRenderUI(NULL);

	RenderScene();

	m_pd3dDevice->SynchronizeToPresentationInterval();

	m_pd3dDevice->Swap( m_pFrontBuffer[ m_dwCurFrontBuffer ], NULL );
	m_dwCurFrontBuffer = ( m_dwCurFrontBuffer + 1 ) % 2;
	
	return S_OK;
}

HRESULT XeUtilitiesApp::Update()
{
	XINPUT_KEYSTROKE keyStroke;
	XInputGetKeystroke( XUSER_INDEX_ANY, XINPUT_FLAG_ANYDEVICE, &keyStroke );

	XeUtilitiesUI::getInstance().DispatchXUIInput( &keyStroke );

	XeUtilitiesUI::getInstance().UpdateUI(NULL);

	return S_OK;
}



HRESULT XeUtilitiesApp::RenderScene()
{
	m_pd3dDevice->SetRenderTarget( 0, m_pBackBuffer );
	m_pd3dDevice->SetDepthStencilSurface( m_pDepthBuffer );

	// Begin Rendering our Scene and all fo its's components
	m_pd3dDevice->BeginTiling( 0, ARRAYSIZE(g_dwTiles), g_dwTiles, &g_vClearColor, 1, 0);
	{

		//Render XUI to the device
		XeUtilitiesUI::getInstance().RenderUI( m_pd3dDevice, Settings::getInstance().GetHorizOverscan(), Settings::getInstance().GetVertOverscan(), g_dwFrameWidth, g_dwFrameHeight );
	}

	m_pd3dDevice->EndTiling( 0, NULL, m_pFrontBuffer[m_dwCurFrontBuffer], NULL, 1, 0, NULL );

	return S_OK;

}