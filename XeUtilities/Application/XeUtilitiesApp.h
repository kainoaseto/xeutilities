#pragma once
#include "Tools\General\Tools.h"

HRESULT XuiTextureLoader(IXuiDevice *pDevice, LPCWSTR szFileName, XUIImageInfo *pImageInfo, IDirect3DTexture9 **ppTex);

static const D3DVECTOR4 g_vClearColor = { 0.0f, 0.0f, 0.0f, 0.0f };
static const DWORD g_dwTileWidth = 1280;
static const DWORD g_dwTileHeight = 256;
static const DWORD g_dwFrameWidth = 1280;
static const DWORD g_dwFrameHeight = 720;
static const D3DRECT g_dwTiles[3] =
{
	{		0,				0, g_dwTileWidth, g_dwTileHeight },
	{		0, g_dwTileHeight, g_dwTileWidth, g_dwTileHeight * 2},
	{		0, g_dwTileHeight * 2, g_dwTileWidth, g_dwFrameHeight },
};


class XeUtilitiesApp : public ATG::Application
{
public:
	static XeUtilitiesApp& getInstance()
	{
		static XeUtilitiesApp singleton;
		return singleton;
	}


protected:
	XeUtilitiesApp();
	~XeUtilitiesApp() {}
	XeUtilitiesApp(const XeUtilitiesApp&); //Prevent copy construction
	XeUtilitiesApp operator=(const XeUtilitiesApp& ); //prevent assignment

	// Rendering surface and textures
	D3DSurface*                 m_pBackBuffer;
    D3DSurface*                 m_pDepthBuffer;
    D3DTexture*                 m_pFrontBuffer[2];
	D3DTexture*					m_pFinalFrontBuffer;
    DWORD                       m_dwCurFrontBuffer;

	// ATG Application interface
	HRESULT Update();
	HRESULT Initialize();
	HRESULT Render();

private:
	HRESULT RenderScene();
	HRESULT CreateRenderTargets( void );
	

};