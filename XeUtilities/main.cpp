#include "stdafx.h"

#include "./Tools/Settings/Settings.h"
#include "./Application/XeUtilitiesApp.h"

//----------------------------------------------------------------------------------
// Name: main
// Desc: Application entry point.
//----------------------------------------------------------------------------------
VOID __cdecl main()
{ 
	DWORD dwLaunchDataSize = 0;    
    DWORD dwStatus = XGetLaunchDataSize( &dwLaunchDataSize );
    if( dwStatus == ERROR_SUCCESS )
    {
        BYTE* pLaunchData = new BYTE [ dwLaunchDataSize ];
        dwStatus = XGetLaunchData( pLaunchData, dwLaunchDataSize );
    }
	
	ATG::GetVideoSettings( &(XeUtilitiesApp::getInstance().m_d3dpp.BackBufferWidth), &(XeUtilitiesApp::getInstance().m_d3dpp.BackBufferHeight) );

	XeUtilitiesApp::getInstance().m_d3dpp.BackBufferCount        = 1;
    XeUtilitiesApp::getInstance().m_d3dpp.MultiSampleType        = D3DMULTISAMPLE_4_SAMPLES;
    XeUtilitiesApp::getInstance().m_d3dpp.EnableAutoDepthStencil = FALSE;
    XeUtilitiesApp::getInstance().m_d3dpp.DisableAutoBackBuffer  = TRUE;
	XeUtilitiesApp::getInstance().m_d3dpp.DisableAutoFrontBuffer = TRUE;
    XeUtilitiesApp::getInstance().m_d3dpp.SwapEffect             = D3DSWAPEFFECT_DISCARD;
	XeUtilitiesApp::getInstance().m_d3dpp.PresentationInterval   = D3DPRESENT_INTERVAL_DEFAULT;

	XeUtilitiesApp::getInstance().m_dwDeviceCreationFlags      |= D3DCREATE_BUFFER_2_FRAMES | D3DCREATE_CREATE_THREAD_ON_0;

	XVIDEO_MODE VideoMode; 
	XMemSet( &VideoMode, 0, sizeof(XVIDEO_MODE) ); 
	XGetVideoMode( &VideoMode );

	
	if (!VideoMode.fIsWideScreen && (Settings::getInstance().GetEnableLetterbox() == 0))
	{
	        XeUtilitiesApp::getInstance().m_d3dpp.Flags |=  D3DPRESENTFLAG_NO_LETTERBOX;
	}
	
	// Run the scene.  
	XeUtilitiesApp::getInstance().Run();
}