#include "stdafx.h"
#include "ScnMain.h"
#include "Application\XeUtilitiesUI.h"
#include "Tools\Settings\Settings.h"
#include "Tools\IniManager\SimpleIni.h"
#include "Tools\FileIO\FileIO.h"
#include "Tools\Xam\HvPeekPoke.h"
#include "Tools\IniManager\IniManager.h"
#include "Tools\Nand\xenon_sfcx.h"

using namespace std;

HRESULT ScnMain::OnInit(XUIMessageInit *pInitData, BOOL& bHandled)
{
	InitializeChildren();
	ReadUserSettings();

	SetTimer(UPDATE_CLOCK, 500);

	Mount("Flash:", "\\Device\\Flash");

	/////Get Cpu Key///////////////////
	wstring wKey = GetCpuKey();
	//wcout << "CPUKEY: " << wKey << "\n";
	LPCWSTR fKey = wKey.c_str();
	CpuKeyText.SetText(fKey);
	//////////////////////////////////

	/////Get Console Type/////////////
	wstring mcType = GetMotherBoard() + L" " + GetConsoleType();
	ConsoleTypeText.SetText(mcType.c_str());
	/////////////////////////////////

	/////Get DVD Key///////////////// DO NOT RUN, Crashes console
	//BYTE  DvdKey[0x10];  // 0x1A
	// Should be 00112233445566778899AABBCCDDEEFA
	//QWORD kvAddress =  HvPeekQWORD(0x00000002000160c0);
	//HvPeekBytes( kvAddress + 0xD0,&DvdKey, 0x40);
	//char cDKey[0x10];
	//CharStr2HexStr(DvdKey, cDKey, 0x10);
	//cout << cDKey << "\n";
	/////////////////////////////////
	unsigned int config;
	if(!sfc.initialized)
	{
		config = sfcx_init();
		sfcx_setconf(config);
		printf("sfc initialized to %i\n", sfc.initialized);
		sfcx_printinfo(config);
	}
	

	wstring fStr = wstring(L"config register: ") + strtowstr(int_to_hex(config));
	NandConfig.SetText(fStr.c_str());

	NandSizeByte.SetText(wstrappend(wstring(L"sfc:size_bytes: "), strtowstr(int_to_hex(sfc.size_bytes))).c_str());
	NandSizePages.SetText(wstrappend(wstring(L"sfc:size_pages: "),strtowstr(int_to_hex(sfc.size_pages))).c_str());
	NandSizeBytePhys.SetText(wstrappend(wstring(L"sfc:size_bytes_phys: "), strtowstr(int_to_hex(sfc.size_bytes_phys))).c_str());
	NandSizeBlocks.SetText(wstrappend(wstring(L"sfc:size_blocks: "), strtowstr(int_to_hex(sfc.size_blocks))).c_str());
	NandPagePhys_sz.SetText(wstrappend(wstring(L"sfc:page_sz_phys: "), strtowstr(int_to_hex(sfc.page_sz_phys))).c_str());
	NandPagesInBlock.SetText(wstrappend(wstring(L"sfc:pages_in_block: "), strtowstr(int_to_hex(sfc.pages_in_block))).c_str());
	NandBlock_sz.SetText(wstrappend(wstring(L"sfc:block_sz: "), strtowstr(int_to_hex(sfc.block_sz))).c_str());
	NandMeta_sz.SetText(wstrappend(wstring(L"sfc:meta_sz: "), strtowstr(int_to_hex(sfc.meta_sz))).c_str());
	NandPage_sz.SetText(wstrappend(wstring(L"sfc:page_sz: "), strtowstr(int_to_hex(sfc.page_sz))).c_str());
	NandSizeMb.SetText(wstrappend(wstring(L"sfc:size_mb: "), sfc.size_mb).c_str());
	NandBlockPhys_sz.SetText(wstrappend(wstring(L"sfc:block_sz_phys: "), strtowstr(int_to_hex(sfc.block_sz_phys))).c_str());

	NandProgressBar.SetRange(0, 100);
	NandProgressBar.SetValue(0);

	return S_OK;
}

LPCWSTR MsgBoxButtons[3] = {L"System Dump", L"Full Dump", L"Cancel"};
MESSAGEBOX_RESULT MsgBoxResult;
BOOL MsgBoxShowing;
WCHAR MsgBoxMsg[256];
XOVERLAPPED MBox_Overlapped;

void NandShowMessageBox(LPCWSTR title, LPCWSTR text)
{
	ATG::SignIn::Initialize( 0, 1, FALSE, 1 );
	ATG::SignIn::Update();
	DWORD dwRet;
	ZeroMemory( &MBox_Overlapped, sizeof( XOVERLAPPED ) );
	printf("User: %d signed in\n", ATG::SignIn::GetSignedInUser());
	dwRet = XShowMessageBoxUI(XUSER_INDEX_ANY, title, text, (DWORD)3, MsgBoxButtons, 0, XMB_ALERTICON, &MsgBoxResult, &MBox_Overlapped);
	ASSERT(dwRet == ERROR_IO_PENDING);
	MsgBoxShowing = TRUE;
	MsgBoxMsg[0] = L'\0';
	
}

HRESULT ScnMain::OnNotifyPress(HXUIOBJ hObjPressed, BOOL& bHandled)
{

	if(hObjPressed == WriteButton)
	{
		int newSpeed;
		XuiSliderGetValue( FanSpeedSlider, &newSpeed);
		XConfig.WriteFanSpeed(0, newSpeed);
		XConfig.WriteFanSpeed(1, newSpeed);
	
		UITools::getInstance().YN_ShowMessageBox(L"Restart Xbox?", L"To finish writing the fanspeed to the SMC the xbox must be restarted. Would you like to restart now?");	
	}

	else if(hObjPressed == LinkerButton)
	{
		LinkInternalDrives();
	}

	else if(hObjPressed == LetterboxCheckbox)
	{
		curLetterbox = LetterboxCheckbox.IsChecked();
		Settings::getInstance().SetEnableLetterbox(curLetterbox);
	}

	else if(hObjPressed == ReadNandButton)
	{
		printf("Read nand pressed\n");
		int size = sfc.size_bytes_phys;
		if((size == (RAW_NAND_64*4)) || (size == (RAW_NAND_64*8)))
		{	
			NandShowMessageBox(L"System or Full?", L"Would you like to dump the entire nand or only the system files?");
		}
	}

	return S_OK;
}

HRESULT ScnMain::OnTimer(XUIMessageTimer *pTimer, BOOL& bHandled)
{
	switch (pTimer->nId)
	{

	case UPDATE_CLOCK:
		UpdateTemps();
		MsgBoxCheck();
		UpdateNand();
		break;
	
	}
	return S_OK;
}

HRESULT ScnMain::OnNotifyValueChanged(HXUIOBJ hObjChanged, XUINotifyValueChanged* pNotifyValueChangedData, BOOL& bHandled)
{
	if(hObjChanged == FanSpeedSlider)
	{
		int newSpeed = pNotifyValueChangedData->nValue;

		if(newSpeed != curSpeed)
		{
			SMC.SetFanSpeed(0, newSpeed);
			SMC.SetFanSpeed(1, newSpeed);

			Settings::getInstance().SetFanSpeed(newSpeed);
			curSpeed = newSpeed;

			if(newSpeed < 26)
			{
				FanSpeedSliderTxt.SetText(L"Auto");
			}
			else
			{
				WCHAR s_newSpeed[256];
				wsprintfW(s_newSpeed, L"%d", newSpeed);
				FanSpeedSliderTxt.SetText(s_newSpeed);
			}
		}
	}
	else if(hObjChanged == CPUTargetSlider)
	{
		int newCPUTarget = pNotifyValueChangedData->nValue;

		if(newCPUTarget != curCPUTarget)
		{
			XConfig.SetCPUThreshhold(newCPUTarget);
			Settings::getInstance().SetCpuThreshhold(newCPUTarget);
			curCPUTarget = newCPUTarget;
		}
	}

	else if(hObjChanged == GPUTargetSlider)
	{
		int newGPUTarget = pNotifyValueChangedData->nValue;

		if(newGPUTarget != curGPUTarget)
		{
			XConfig.SetGPUThreshhold(newGPUTarget);
			Settings::getInstance().SetGpuThreshhold(newGPUTarget);
			curGPUTarget = newGPUTarget;
		}
	}

	else if(hObjChanged == MemTargetSlider)
	{
		int newMemTarget = pNotifyValueChangedData->nValue;

		if(newMemTarget != curMemTarget)
		{
			XConfig.SetMemThreshhold(newMemTarget);
			Settings::getInstance().SetMemThreshhold(newMemTarget);
			curMemTarget = newMemTarget;
		}
	}

	else if(hObjChanged == VertOverscanSlider)
	{
		int newVertOverscan = pNotifyValueChangedData->nValue;

		if(newVertOverscan != curVertOverscan)
		{
			Settings::getInstance().SetVertOverscan(newVertOverscan);
			curVertOverscan = newVertOverscan;
		}
	}

	else if(hObjChanged == HorizOverscanSlider)
	{
		int newHorizOverscan = pNotifyValueChangedData->nValue;

		if(newHorizOverscan != curHorizOverscan)
		{
			Settings::getInstance().SetHorizOverscan(newHorizOverscan);
			curHorizOverscan = newHorizOverscan;
		}
	}



	else if(hObjChanged == topLeftRol)
	{
		if(SMC.GetTiltState()) // Console is horizontal
		{
			
		}

		else // Console is vertical
		{

		}
	}

	else if(hObjChanged == topRightRol)
	{

	}

	else if(hObjChanged == bottomLeftRol)
	{

	}

	else if(hObjChanged == bottomRightRol)
	{

	}

	bHandled = TRUE;

	return S_OK;
}

HRESULT ScnMain::OnNotifySelChanged(HXUIOBJ hObjSel, XUINotifySelChanged* pNotifySelChagnedData, BOOL& bHandled)
{
	return S_OK;
}

HRESULT ScnMain::UpdateNand()
{
	//printf("UpdateNand(), %i\n", ((float)(getCurNumBlocks() + 1) / (float)getTotalNumBlocks()) * 100);
	if(getReadingNandBasic())
	{
		CurrentNandProcess.SetText((wstrappend(wstrappend(wstrappend(wstring(L"Processing Block 0x"), strtowstr(int_to_hex(getCurNumBlocks()))), L" of 0x"), strtowstr(int_to_hex(getTotalNumBlocks())))).c_str());
		NandProgressBar.SetValue((int)(((float)(getCurNumBlocks() + 1) / (float)getTotalNumBlocks()) * 100));
	}
	return S_OK;
}

HRESULT ScnMain::MsgBoxCheck()
{
	if( UITools::getInstance().YN_MsgBoxShowing )
	{
		if( XHasOverlappedIoCompleted( &UITools::getInstance().YN_Overlapped ) )
		{
			UITools::getInstance().YN_MsgBoxShowing = FALSE;
			DWORD dwResult = XGetOverlappedResult( &UITools::getInstance().YN_Overlapped, NULL, TRUE );
			if( dwResult == ERROR_SUCCESS )
			{
				switch((int)UITools::getInstance().YN_Result.dwButtonPressed)
				{
				case(1):
					printf("User chose not to restart\n");
					break;
				case(0):
					printf("User chose to restart\n");
					HalReturnToFirmware(HalRebootQuiesceRoutine);
					break;
				default:
					printf("Unexpected Result\n");
				}
			}
			else if( dwResult == ERROR_CANCELLED )
			{
				swprintf_s( UITools::getInstance().YN_Msg, L"MessageBox was cancelled by user.\n" );
			}
			else
			{
				swprintf_s( UITools::getInstance().YN_Msg, L"MessageBox completed with failure 0x%08x.\n", dwResult );
			}

		}
	}

	if( MsgBoxShowing )
	{
		if( XHasOverlappedIoCompleted( &MBox_Overlapped ) )
		{
			MsgBoxShowing = FALSE;
			DWORD dwResult = XGetOverlappedResult( &MBox_Overlapped, NULL, TRUE );
			if( dwResult == ERROR_SUCCESS )
			{
				switch((int)MsgBoxResult.dwButtonPressed)
				{
				case(2):
					printf("Cancel");
					break;
				case(1):
					printf("Full dump");
					Nand.ReadNand(false);
					break;
				case(0):
					printf("System Dump\n");
					Nand.ReadNand(true);
					
					break;
				default:
					printf("Unexpected Result\n");
				}
			}
			else if( dwResult == ERROR_CANCELLED )
			{
				swprintf_s( MsgBoxMsg, L"MessageBox was cancelled by user.\n" );
			}
			else
			{
				swprintf_s( MsgBoxMsg, L"MessageBox completed with failure 0x%08x.\n", dwResult );
			}

		}
	}
			
	return S_OK;
}

void ScnMain::UpdateTemps()
{
	float fTemp;
	string tempString;

	TempTxts[0] = CpuTempTxt;
	TempTxts[1] = GpuTempTxt;
	TempTxts[2] = MemTempTxt;
	TempTxts[3] = MBTempTxt;

	for(int i = 0; i < 4; i++)
	{
		fTemp = SMC.GetTemperature((TEMP_INDEX)i, true);
		tempString = sprintfa("%0.1f", fTemp);
		TempTxts[i].SetText(strtowstr(tempString).c_str());
	}

}

HRESULT ScnMain::InitializeChildren( void )
{
	// Initialize Fanspeed controls
	GetChildById(L"MemTargetSlider", &MemTargetSlider);
	GetChildById(L"CPUTargetSlider", &CPUTargetSlider);
	GetChildById(L"GPUTargetSlider", &GPUTargetSlider);
	GetChildById(L"FanSpeedSlider", &FanSpeedSlider);
	GetChildById(L"FanSpeedSliderText", &FanSpeedSliderTxt);
	GetChildById(L"WriteButton", &WriteButton );
	GetChildById(L"CPUTemp", &CpuTempTxt);
	GetChildById(L"GPUTemp", &GpuTempTxt);
	GetChildById(L"MemTemp", &MemTempTxt);
	GetChildById(L"MBTemp", &MBTempTxt);

	//Initialize ROL controls
	GetChildById(L"TopLeftROL", &topLeftRol);
	GetChildById(L"TopRightROL", &topRightRol);
	GetChildById(L"BottomLeftROL", &bottomLeftRol);
	GetChildById(L"BottomRightROL", &bottomRightRol);

	// Initialize Nand controls
	GetChildById(L"InternalLinker",&LinkerButton);
	GetChildById(L"ReadNandButton", &ReadNandButton);
	GetChildById(L"WriteNandButton", &WriteNandButton);

	GetChildById(L"NandSizeByte", &NandSizeByte);
	GetChildById(L"NandSizePages", &NandSizePages);
	GetChildById(L"NandSizeBytePhys", &NandSizeBytePhys);
	GetChildById(L"NandSizeBlocks", &NandSizeBlocks);
	GetChildById(L"NandPagePhys", &NandPagePhys_sz);
	GetChildById(L"NandPagesInBlock", &NandPagesInBlock);
	GetChildById(L"NandBlock", &NandBlock_sz);
	GetChildById(L"NandMeta", &NandMeta_sz);
	GetChildById(L"NandPage", &NandPage_sz);
	GetChildById(L"NandConfig", &NandConfig);
	GetChildById(L"NandSizeMb", &NandSizeMb);
	GetChildById(L"NandBlockPhys", &NandBlockPhys_sz);

	GetChildById(L"CurrentNandProcess", &CurrentNandProcess);
	GetChildById(L"NandProgress", &NandProgressBar);

	// Initialize Settings controls
	GetChildById(L"VertOverscan", &VertOverscanSlider);
	GetChildById(L"HorizOverscan", &HorizOverscanSlider);
	GetChildById(L"LetterboxCheckbox", &LetterboxCheckbox);

	//Summary
	GetChildById(L"CPUKey", &CpuKeyText);
	GetChildById(L"ConsoleType", &ConsoleTypeText);

	return S_OK;
}

HRESULT ScnMain::ReadUserSettings(void )
{
	FanSpeedSlider.SetValue(Settings::getInstance().GetFanSpeed());
	CPUTargetSlider.SetValue(Settings::getInstance().GetCpuThreshhold());
	GPUTargetSlider.SetValue(Settings::getInstance().GetGpuThreshhold());
	MemTargetSlider.SetValue(Settings::getInstance().GetMemThreshhold());

	VertOverscanSlider.SetValue(Settings::getInstance().GetVertOverscan());
	HorizOverscanSlider.SetValue(Settings::getInstance().GetHorizOverscan());
	LetterboxCheckbox.SetCheck(Settings::getInstance().GetEnableLetterbox());

	return S_OK;
}