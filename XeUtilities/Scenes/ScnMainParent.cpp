#include "stdafx.h"
#include "ScnMainParent.h"

ScnMainParent::ScnMainParent()
{

}

ScnMainParent::~ScnMainParent()
{

}

HRESULT ScnMainParent::OnInit(XUIMessageInit *pInitData, BOOL& bHandled)
{
	InitializeChildren();

	currentSel = 0;
	ShowCurItem(currentSel);
	m_InfoScenes[currentSel].SetFocus();

	return S_OK;
}

HRESULT ScnMainParent::OnNotifySelChanged( HXUIOBJ hObjSource, XUINotifySelChanged* pNotifySelChagnedData, BOOL& bHandled )
{
	if( hObjSource == m_InfoList )
	{
		ShowCurItem(m_InfoList.GetCurSel());
		currentSel = m_InfoList.GetCurSel();	
	}

	else {}

	return S_OK;
}

HRESULT ScnMainParent::OnNotifyPress( HXUIOBJ hObjPressed, BOOL& bHandled )
{
	if( hObjPressed == m_InfoList )
	{
		m_InfoScenes[currentSel].SetFocus();
		isChild = true;
		bHandled = TRUE;
	}

	else {}

	return S_OK;
}

HRESULT ScnMainParent::OnNotifySetFocus( HXUIOBJ hObjSource, XUINotifyFocus *pNotifyFocusData, BOOL& bHandled )
{
	if(hObjSource == m_MainFocus)
	{
		m_InfoScenes[currentSel].SetFocus();
		isChild = true;
	}
	else if(hObjSource == m_InfoList)
	{
		isChild = false;
	}

	bHandled = TRUE;

	return S_OK;
}

HRESULT ScnMainParent::InitializeChildren( void )
{
	GetChildById(L"InformationList", &m_InfoList);
	GetChildById(L"MainFocus", &m_MainFocus);
	GetChildById(L"SMCScene", &m_InfoScenes[0]);
	GetChildById(L"NandScene", &m_InfoScenes[1]);
	GetChildById(L"PluginScene", &m_InfoScenes[2]);
	GetChildById(L"NetworkScene", &m_InfoScenes[3]);
	GetChildById(L"SpoofIdsScene", &m_InfoScenes[4]);
	GetChildById(L"HypervisorScene", &m_InfoScenes[5]);
	GetChildById(L"SettingsScene", &m_InfoScenes[6]);
	GetChildById(L"AboutScene", &m_InfoScenes[7]);

	return S_OK;
}

void ScnMainParent::ShowCurItem(int value)
{
	for(int i = 0; i < InfoListSize; i++)
	{
		if(i == value)
		{
			//printf("i equals value: %i\n", m_InfoList.GetCurSel());
			m_InfoScenes[i].SetShow(true);
		}

		else
		{
			//printf("%i != value: %i\n", i, m_InfoList.GetCurSel());
			m_InfoScenes[i].SetShow(false);
		}
	}
}