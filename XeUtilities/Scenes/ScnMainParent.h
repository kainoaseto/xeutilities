#pragma once
#include"stdafx.h"

class ScnMainParent : CXuiSceneImpl
{
public:

	ScnMainParent();
	~ScnMainParent();
	
	enum ContentType
	{
		SMC,
		Nand,
		Plugins,
		Network,
		SpoofIds,
		Hypervisor,
		Settings,
		About,
		InfoListSize
	};

	CXuiList m_InfoList;
	CXuiControl m_MainFocus;

	CXuiScene m_InfoScenes[8];

	int currentSel;
	bool isChild;

	// XUI Map

	XUI_IMPLEMENT_CLASS( ScnMainParent, L"ScnMainParent", XUI_CLASS_SCENE )

	XUI_BEGIN_MSG_MAP()
		XUI_ON_XM_INIT( OnInit )
		XUI_ON_XM_NOTIFY_PRESS( OnNotifyPress )
		XUI_ON_XM_NOTIFY_SELCHANGED( OnNotifySelChanged )
		XUI_ON_XM_NOTIFY_SET_FOCUS( OnNotifySetFocus )
	XUI_END_MSG_MAP()

	HRESULT OnInit(XUIMessageInit *pInitData, BOOL& bHandled);
	HRESULT OnNotifyPress( HXUIOBJ hObjPressed, BOOL& bHandled);
	HRESULT OnNotifySelChanged(HXUIOBJ hObjSource, XUINotifySelChanged* pNotifySelChangedData, BOOL& bHandled );
	HRESULT OnNotifySetFocus(HXUIOBJ hObjSource, XUINotifyFocus *pNotifyFocusData, BOOL& bHandled );

	HRESULT InitializeChildren( void );
	void ShowCurItem(int value);

};