#pragma once

#include "stdafx.h"
#include "Tools\General\UITools.h"
#include "Tools\Kernel\smc.h"
#include "Tools\Kernel\XConfig.h"
#include "Tools\FileIO\FileIO.h"
#include "Tools\Nand\nand.h"

#define UPDATE_CLOCK 321

class ScnMain : CXuiSceneImpl
{

public:

	ScnMain() {}
	~ScnMain() {}

	CXuiTextElement TempTxts[4];
	// SMC
	////Fanspeed
		CXuiControl WriteButton;
		CXuiSlider  FanSpeedSlider;
		CXuiSlider  CPUTargetSlider;
		CXuiSlider  GPUTargetSlider;
		CXuiSlider  MemTargetSlider;
		CXuiTextElement FanSpeedSliderTxt;

		CXuiTextElement MBTempTxt;
		CXuiTextElement MemTempTxt;
		CXuiTextElement CpuTempTxt;
		CXuiTextElement GpuTempTxt;

		int curSpeed;
		int curCPUTarget;
		int curGPUTarget;
		int curMemTarget;

	////ROL
		CXuiComboBox topLeftRol;
		CXuiComboBox topRightRol;
		CXuiComboBox bottomLeftRol;
		CXuiComboBox bottomRightRol;

		int curTopLeft;
		int curTopRight;
		int curBotLeft;
		int curBotRight;
	

	// Nand
	CXuiControl LinkerButton;
	CXuiControl ReadNandButton;
	CXuiControl WriteNandButton;

	CXuiTextElement NandSizeByte;
	CXuiTextElement NandSizePages;
	CXuiTextElement NandSizeBytePhys;
	CXuiTextElement NandSizeBlocks;
	CXuiTextElement NandPagePhys_sz;
	CXuiTextElement NandPagesInBlock;
	CXuiTextElement NandBlock_sz;
	CXuiTextElement NandMeta_sz;
	CXuiTextElement NandPage_sz;
	CXuiTextElement NandConfig;
	CXuiTextElement NandSizeMb;
	CXuiTextElement NandBlockPhys_sz;

	CXuiTextElement CurrentNandProcess;
	CXuiProgressBar NandProgressBar;

	// Settings
	CXuiSlider VertOverscanSlider;
	CXuiSlider HorizOverscanSlider;
	CXuiCheckbox LetterboxCheckbox;
	int curVertOverscan;
	int curHorizOverscan;
	int curLetterbox;

	//Summary
	CXuiTextElement CpuKeyText;
	CXuiTextElement ConsoleTypeText;

	//XUI msg map

	XUI_IMPLEMENT_CLASS( ScnMain, L"ScnMain", XUI_CLASS_SCENE )

	XUI_BEGIN_MSG_MAP()
		XUI_ON_XM_INIT(OnInit)
		XUI_ON_XM_NOTIFY_PRESS( OnNotifyPress )
		XUI_ON_XM_TIMER( OnTimer )
		XUI_ON_XM_NOTIFY_VALUE_CHANGED( OnNotifyValueChanged )
		XUI_ON_XM_NOTIFY_SELCHANGED( OnNotifySelChanged )
	XUI_END_MSG_MAP()

	HRESULT OnInit(XUIMessageInit *pInitData, BOOL& bHandled);
	HRESULT OnNotifyPress(HXUIOBJ hObjPressed, BOOL& bHandled);
	HRESULT OnTimer(XUIMessageTimer *pTimer, BOOL& bHandled);
	HRESULT OnNotifyValueChanged(HXUIOBJ hObjChanged, XUINotifyValueChanged* pNotifyValueChagnedData, BOOL& bHandled);
	HRESULT OnNotifySelChanged(HXUIOBJ hObjSel, XUINotifySelChanged* pNotifySelChagnedData, BOOL& bHandled);

	HRESULT InitializeChildren( void );
	HRESULT ReadUserSettings(void );
	
private:
	smc SMC;
	XConfig XConfig;
	Nand Nand;

	HRESULT MsgBoxCheck( void );
	void UpdateTemps( void );
	HRESULT UpdateNand( void );
	
};