// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently


#pragma once

#include <stdlib.h> 
#include <xtl.h>
#include <xui.h>
#include <xuiapp.h>

#include <AtgApp.h>
#include <AtgFont.h>
#include <AtgInput.h>
#include <AtgUtil.h>
#include <AtgMediaLocator.h>
#include <AtgSignIn.h>

#include <iostream>
#include <iomanip>
#include <sstream>
#include <string.h>
#include <string>
#include <time.h>
#include <d3d9.h>
#include <stdio.h>
#include <vector>
#include <algorithm>

#include <xkelib.h>

#pragma warning(disable:4652) // useing C++ exception handling
