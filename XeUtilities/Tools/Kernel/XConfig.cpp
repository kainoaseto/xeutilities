#include "stdafx.h"
#include "XConfig.h"

XConfig::XConfig() {}

XConfig::~XConfig() {}

UINT XConfig::XConfigStaticMainChecksumCalc(PBYTE data)
{
	UINT i, len, sum = 0;
	data += 0x10;
	for(i=0, len=252; i<len; i++)
		sum += data[i]&0xFF;
	sum = (~sum)&0xFFFF;
	return ((sum&0xFF00)<<8)+((sum&0xFF)<<24);
}

NTSTATUS XConfig::SetCPUThreshhold(BYTE cpu)
{
	XCONFIG_STATIC_SETTINGS xcss;
	DWORD sta;
	WORD sz;
	sta = ExGetXConfigSetting(XCONFIG_STATIC_CATEGORY, XCONFIG_STATIC_DATA, &xcss, sizeof(XCONFIG_STATIC_SETTINGS), &sz);
	if((sta == 0) && (sz == sizeof(XCONFIG_STATIC_SETTINGS)))
	{
		xcss.SMCConfig.Temperature.SetPoint.Cpu = cpu;
		xcss.CheckSum = XConfigStaticMainChecksumCalc((PBYTE)&xcss);
		sta = ExSetXConfigSetting(XCONFIG_STATIC_CATEGORY, XCONFIG_STATIC_DATA, &xcss, sz);
	}
	return sta;
}

NTSTATUS XConfig::SetGPUThreshhold(BYTE gpu)
{
	XCONFIG_STATIC_SETTINGS xcss;
	DWORD sta;
	WORD sz;
	sta = ExGetXConfigSetting(XCONFIG_STATIC_CATEGORY, XCONFIG_STATIC_DATA, &xcss, sizeof(XCONFIG_STATIC_SETTINGS), &sz);
	if((sta == 0) && (sz == sizeof(XCONFIG_STATIC_SETTINGS)))
	{
		xcss.SMCConfig.Temperature.SetPoint.Gpu = gpu;
		xcss.CheckSum = XConfigStaticMainChecksumCalc((PBYTE)&xcss);
		sta = ExSetXConfigSetting(XCONFIG_STATIC_CATEGORY, XCONFIG_STATIC_DATA, &xcss, sz);
	}
	return sta;
}

NTSTATUS XConfig::SetMemThreshhold(BYTE mem)
{
	XCONFIG_STATIC_SETTINGS xcss;
	DWORD sta;
	WORD sz;
	sta = ExGetXConfigSetting(XCONFIG_STATIC_CATEGORY, XCONFIG_STATIC_DATA, &xcss, sizeof(XCONFIG_STATIC_SETTINGS), &sz);
	if((sta == 0) && (sz == sizeof(XCONFIG_STATIC_SETTINGS)))
	{
		xcss.SMCConfig.Temperature.SetPoint.Edram = mem;
		xcss.CheckSum = XConfigStaticMainChecksumCalc((PBYTE)&xcss);
		sta = ExSetXConfigSetting(XCONFIG_STATIC_CATEGORY, XCONFIG_STATIC_DATA, &xcss, sz);
	}
	return sta;
}

	
void XConfig::WriteFanSpeed(int fan, int speed) // Fan 0 = CPU, 1 = GPU
{
	unsigned char cSpeed = (unsigned char)speed;
	u16 SettingSize;
	XCONFIG_STATIC_SETTINGS xcss;
	
	ExGetXConfigSetting(XCONFIG_STATIC_CATEGORY, XCONFIG_STATIC_DATA, NULL, NULL, &SettingSize);
	ExGetXConfigSetting(XCONFIG_STATIC_CATEGORY, XCONFIG_STATIC_DATA, &xcss, SettingSize, NULL);

	if(fan)
	{
		xcss.SMCConfig.fanOrGpu.Enable = 1;
		xcss.SMCConfig.fanOrGpu.Speed = cSpeed;
	}

	else
	{
		xcss.SMCConfig.fanOrCpu.Enable = 1;
		xcss.SMCConfig.fanOrCpu.Speed = cSpeed;
		
	}

	xcss.CheckSum = XConfigStaticMainChecksumCalc((BYTE*)&xcss);
	ExSetXConfigSetting(XCONFIG_STATIC_CATEGORY, XCONFIG_STATIC_DATA, &xcss, SettingSize);
}