#pragma once
#include "Application\XeUtilitiesUI.h"
#include "Tools\General\Tools.h"
#include "stdafx.h"
using namespace std;

class SceneManager
{
public:
	static SceneManager& getInstance()
	{
		static SceneManager singleton;
		return singleton;
	}


	void SetScene(std::string sceneName, HXUIOBJ curScene, bool stayVisible, std::string objectRef = "", void * pvInitData = 0);

	void SetSkin( XeUtilitiesUI& g_uiApp, std::string firstScene);

private:

	SceneManager() {}
	~SceneManager() {}
	SceneManager(const SceneManager&);
	SceneManager& operator=(const SceneManager&);

	void LoadSkin( XeUtilitiesUI& g_uiApp, std::string firstScene);
	void LoadScene(std::string sceneName, HXUIOBJ curScene, bool stayVisible, std::string objectRef = "", void * pvInitData = 0);
};