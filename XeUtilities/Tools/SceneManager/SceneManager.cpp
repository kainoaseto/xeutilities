#include "stdafx.h"
#include "SceneManager.h"
#include "Tools\General\Tools.h"
#include "Application\XeUtilitiesApp.h"
#include "Application\XeUtilitiesUI.h"

void SceneManager::SetSkin( XeUtilitiesUI& g_uiApp, string firstScene = "Main.xur")
{
	LoadSkin(g_uiApp, firstScene);
}

void SceneManager::SetScene( string sceneName, HXUIOBJ curScene, bool stayVisible, std::string objectRef, void * pvInitData )
{
	LoadScene(sceneName, curScene, stayVisible, objectRef, pvInitData);
}

void SceneManager::LoadSkin(XeUtilitiesUI& g_uiApp, string firstScene)
{
	HRESULT hr;

	/*printf(wstrtostr(XuiGetLocale()).c_str());
	XuiSetLocale(XuiGetLocale());
	XuiApplyLocale(g_uiApp, NULL);*/

	hr = g_uiApp.LoadSkin( L"file://game:/Skins/Default.xzp#Skin.xur" );
	printf("Skin HR: %08x\n", hr);
	if( hr != S_OK )
	{
		printf("Skin failed to load: %08x", hr);
		return;
	}

	hr = g_uiApp.LoadFirstScene( L"file://game:/Skins/Default.xzp#", L"Main.xur" );
	if( hr != S_OK )
	{
		printf("Failed to load firstScene");
		return;
	}

	
}

void SceneManager::LoadScene( string sceneName, HXUIOBJ curScene, bool stayVisible, string objectRef, void * pvInitData )
{
	HXUIOBJ hScene;

	CXuiScene::SceneCreate( L"file://game:/Skins/Default.xzp#", strtowstr(sceneName).c_str(), &hScene, pvInitData);

	if(objectRef == "")
	{
		objectRef = sceneName;
	}

	XuiSceneNavigateForward(curScene, stayVisible, hScene, XUSER_INDEX_ANY);
}