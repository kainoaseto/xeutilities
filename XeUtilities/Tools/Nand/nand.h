#pragma once
#include "Tools\General\UITools.h"
#include "Tools\Threads\CThread.h"
class Nand : CThread
{
public:
	Nand();
	~Nand();


	void WriteNand();
	void ReadNand(bool isSystemDump);
};
