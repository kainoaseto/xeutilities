#include "stdafx.h"
#include <stdint.h>
#include "Corona4G.h"

#define BUF_SIZE	0x8000
DWORD flash_sz = 0, TTL_REPS = 0, totalBytes = 0, totalRead = 0, totalWrote = 0;
BYTE buf[BUF_SIZE];
char* file_loc = "";

HRESULT mount(const char* szDrive, char* szDevice)
{
	STRING DeviceName, LinkName;
	CHAR szDestinationDrive[MAX_PATH];
	sprintf_s(szDestinationDrive, MAX_PATH, "\\??\\%s", szDrive);
	RtlInitAnsiString(&DeviceName, szDevice);
	RtlInitAnsiString(&LinkName, szDestinationDrive);
	ObDeleteSymbolicLink(&LinkName);
	return (HRESULT)ObCreateSymbolicLink(&LinkName, &DeviceName);
}

char* GetSizeReadable(unsigned int i)
{
	char * ret = "";
	if (i >= 0x40000000) // Gigabyte
		sprintf_s(ret, 512, "%3.2f GB", (float)(i >> 20) / (float)1024);
	else if (i >= 0x100000) // Megabyte
		sprintf_s(ret, 512, "%3.2f MB", (float)(i >> 10) / (float)1024);
	else if (i >= 0x400) // Kilobyte
		sprintf_s(ret, 512, "%3.2f KB", (float)i / (float)1024);
	else
		sprintf_s(ret, 512, "%3.2f B", (float)i / (float)1024);
	return ret;
}

int getflashsz(void)
{
	FILE * fd;
	printf("Getting nand size...\n");
	if (mount("Flash:", "\\Device\\Flash") != 0)
	{
		printf("Unable to mount flash!\n");
		return 0;
	}
	if (fopen_s(&fd, "Flash:", "rb") != 0)
	{
		printf("Unable to open flash for reading!\n");
		fclose(fd);
		return 0;
	}
	fseek(fd, 0L, SEEK_END);
	flash_sz = ftell(fd);
	fclose(fd);
	printf(" * Size: 0x%X (%s)\n", flash_sz, GetSizeReadable(flash_sz));
	return 1;
}
ReadCorona4G::ReadCorona4G(char* filename)
{
	file_loc = filename;
	CreateThread(CPU3_THREAD_2);
}

unsigned long ReadCorona4G::Process(void* parameter)
{
	SetThreadName("ReadNandCorona4G");
	HANDLE flash, outf;
	DWORD bRead, bWrote;
	DWORD i;
	totalRead = 0;
	totalWrote = 0;
	printf(" * rawdump4g v1 started (by Swizzy)\n");
	if (getflashsz() == 0)
	{
		printf("Unable to get size of flash");
		return 0;
	}
	TTL_REPS = (flash_sz/BUF_SIZE);
	flash = CreateFile("Flash:", GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);	
	if(flash == INVALID_HANDLE_VALUE)
	{
		printf("Unable to open flash for reading!");
		return 0;
	}
	outf = CreateFile(file_loc, GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if(outf == INVALID_HANDLE_VALUE)
	{
		printf("Unable to open %s for writing\n", file_loc);
		return 0;
	}
	printf("%s opened OK, attempting to read 0x%X bytes (%s) from flash...\n", file_loc, flash_sz, GetSizeReadable(flash_sz));
	for(i = 0; i < TTL_REPS; i++)
	{
		ReadFile(flash, buf, BUF_SIZE, &bRead, NULL);
		if(bRead != BUF_SIZE)
		{
			printf("\rerror reading 0x%X bytes at offset 0x%X (read: 0x%X bytes)!\n", BUF_SIZE, totalBytes, bRead);
		}
		WriteFile(outf, buf, BUF_SIZE, &bWrote, NULL);
		if(bWrote != BUF_SIZE)
		{
			printf("\rerror writing 0x%X bytes at offset 0x%X (wrote: 0x%X bytes)!\n", BUF_SIZE, totalBytes, bWrote);
		}
		totalBytes += BUF_SIZE;
		totalRead += bRead;
		totalWrote += bWrote;
		if(i%0x20) // only update this every 32 buffer fills to speed things along...
		{
			printf("\rProcessed 0x%X of 0x%X bytes (%3.2f %%)", totalBytes, flash_sz, (((float)totalBytes / (float)flash_sz) * 100));
		}		
	}
	printf("\r");
	CloseHandle(outf);
	CloseHandle(flash);
	if (totalRead - totalWrote == 0)
	{
		printf("\nDone! successfully dumped 0x%X bytes to %s\n", totalBytes, file_loc);
	}
	else
	{
		printf("\nDone! read 0x%X bytes from NAND, wrote 0x%X bytes to %s\n", totalRead, totalWrote, file_loc);
	}
	return 1;
}
/*void try_rawdump4g(char* filename)
{
	HANDLE flash, outf;
	DWORD bRead, bWrote;
	DWORD i;
	totalRead = 0;
	totalWrote = 0;
	printf(" * rawdump4g v1 started (by Swizzy)\n");
	if (getflashsz() == 0)
	{
		printf("Unable to get size of flash");
		return;
	}
	TTL_REPS = (flash_sz/BUF_SIZE);
	flash = CreateFile("Flash:", GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);	
	if(flash == INVALID_HANDLE_VALUE)
	{
		printf("Unable to open flash for reading!");
		return;
	}
	outf = CreateFile(filename, GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if(outf == INVALID_HANDLE_VALUE)
	{
		printf("Unable to open %s for writing\n", filename);
		return;
	}
	printf("%s opened OK, attempting to read 0x%X bytes (%s) from flash...\n", filename, flash_sz, GetSizeReadable(flash_sz));
	for(i = 0; i < TTL_REPS; i++)
	{
		ReadFile(flash, buf, BUF_SIZE, &bRead, NULL);
		if(bRead != BUF_SIZE)
		{
			printf("\rerror reading 0x%X bytes at offset 0x%X (read: 0x%X bytes)!\n", BUF_SIZE, totalBytes, bRead);
		}
		WriteFile(outf, buf, BUF_SIZE, &bWrote, NULL);
		if(bWrote != BUF_SIZE)
		{
			printf("\rerror writing 0x%X bytes at offset 0x%X (wrote: 0x%X bytes)!\n", BUF_SIZE, totalBytes, bWrote);
		}
		totalBytes += BUF_SIZE;
		totalRead += bRead;
		totalWrote += bWrote;
		if(i%0x20) // only update this every 32 buffer fills to speed things along...
		{
			printf("\rProcessed 0x%X of 0x%X bytes (%3.2f %%)", totalBytes, flash_sz, (((float)totalBytes / (float)flash_sz) * 100));
		}		
	}
	printf("\r");
	CloseHandle(outf);
	CloseHandle(flash);
	if (totalRead - totalWrote == 0)
	{
		printf("\nDone! successfully dumped 0x%X bytes to %s\n", totalBytes, filename);
	}
	else
	{
		printf("\nDone! read 0x%X bytes from NAND, wrote 0x%X bytes to %s\n", totalRead, totalWrote, filename);
	}
}*/

void try_rawflash4g(char* filename)
{
	PBYTE data;
	DWORD filesize;
	printf(" * rawflash4g v1 started (by Swizzy)\n");	
	if (getflashsz() == 0)
	{
		printf("Unable to get size of flash");
		return;
	}
	printf("Reading to memory");
	data = FileToBuffer(filename, &filesize);
	if (data == NULL)
		return;
	if (flash_sz < filesize)
	{
		printf("%s is bigger then Flash! Aborting!\n", filename);
		free(data);
		return;
	}
	if (flash_sz > filesize)
	{
		printf("%s is smaller then Flash! Aborting!\n", filename);
		free(data);
		return;
	}
	printf("Writing to flash");
	if (Corona4GWrite(data))
	{
		printf("\nDone! successfully dumped 0x%X bytes to %s\n", filesize, filename);
	}
	else
	{
		printf("\nDone! Failed to dump 0x%X bytes to %s\n", filesize, filename);
	}
}

BOOL Corona4GWrite(PBYTE data)
{
	HANDLE hFile;
	OBJECT_ATTRIBUTES atFlash;
	IO_STATUS_BLOCK ioFlash;
	DWORD sta = 0, bWrote = 0, len = flash_sz;
	PBYTE srcdata = data;
	LARGE_INTEGER lOffset;
	STRING nFlash;
	BOOL error;
	RtlInitAnsiString(&nFlash, "\\Device\\Flash");
	atFlash.RootDirectory = 0;
	atFlash.ObjectName = &nFlash;
	atFlash.Attributes = FILE_ATTRIBUTE_DEVICE;
	if (NtOpenFile(&hFile, GENERIC_WRITE|GENERIC_READ|SYNCHRONIZE, &atFlash, &ioFlash, OPEN_EXISTING, FILE_SYNCHRONOUS_IO_NONALERT) != 0)
	{
		printf("Unable to open flash for writing");
		return FALSE;
	}
	lOffset.QuadPart = 0;
	error = FALSE;
	while (len > 0x4000)
	{
		KillControllers();
		bWrote = 0;
		sta = NtWriteFile(hFile, 0, 0, 0, &ioFlash, srcdata, 0x4000, &lOffset);
		if (sta < 0)
		{
			printf("\rUnable to write 0x%X bytes to flash, ERROR: 0x%X Offset: 0x%X", 0x4000, GetLastError(), lOffset.LowPart);
			error = TRUE;
		}
		else
			bWrote = 0x4000;
		len -= bWrote;
		srcdata += bWrote;
		lOffset.QuadPart += bWrote;
		if ((lOffset.LowPart%(0x4000*32)) == 0)
		{
			printf("\rProcessed 0x%X of 0x%X bytes (%3.2f %%)", lOffset.LowPart, flash_sz, (((float)lOffset.LowPart / (float)flash_sz) * 100));
		}
	}
	printf("\r");
	if (len > 0)
	{
		bWrote = 0;
		sta = NtWriteFile(hFile, 0, 0, 0, &ioFlash, srcdata, len, &lOffset);
		if (sta < 0)
		{
			printf("\rUnable to write 0x%X bytes to flash, ERROR: 0x%X Offset: 0x%X", len, GetLastError(), lOffset.LowPart);
			error = TRUE;
		}
		else
			bWrote = len;
		lOffset.QuadPart += bWrote;
	}
	NtClose(hFile);
	if (error)
		return FALSE;
	return TRUE;
}

PBYTE FileToBuffer(char * file, PDWORD filesize)
{
	HANDLE inf;
	PBYTE dest = NULL;
	DWORD bRead, lSize;
	filesize[0] = 0;
	inf = CreateFile(file, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (inf == INVALID_HANDLE_VALUE)
	{
		printf("Unable to open %s for reading, error: 0x%X\n", file, GetLastError());
		return dest;
	}
	lSize = GetFileSize(inf, NULL);
	dest = (PBYTE)malloc(lSize);
	if (dest == NULL)
	{
		printf("Unable to allocate 0x%X bytes buffer for %s\n", lSize, file);
		CloseHandle(inf);
		return dest;
	}
	ReadFile(inf, dest, lSize, &bRead, NULL);
	if (bRead != lSize)
	{
		printf("Unable to read 0x%X bytes from %s (read: 0x%X bytes)\n", lSize, file, bRead);
		CloseHandle(inf);
		return NULL;
	}
	CloseHandle(inf);
	filesize[0] = lSize;
	return dest;
}