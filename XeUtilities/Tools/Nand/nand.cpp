#include "stdafx.h"
#include "Tools\General\UITools.h"
#include "Tools\FileIO\FileIO.h"
#include "Tools\General\Tools.h"
#include <time.h>
#include "Corona4G.h"

#include "nand.h"


#include "xenon_sfcx.h"


Nand::Nand()
{
}

Nand::~Nand()
{
}

ATG::GAMEPAD*	m_pGamepad; // Gamepad for input
time_t start,end; //Timer times for measuring time difference
double tdiff; // Double for time difference
bool started = false, dumped = false, dump_in_progress = false, MMC = false, write = false, AutoMode = false;
unsigned int config = 0;

//extern "C" VOID HalReturnToFirmware(DWORD mode); // To Shutdown console when done ;)
extern "C" VOID XInputdPowerDownDevice(DWORD flag); // To Kill controllers
static unsigned long bswap32(unsigned long t) { return ((t & 0xFF) << 24) | ((t & 0xFF00) << 8) | ((t & 0xFF0000) >> 8) | ((t & 0xFF000000) >> 24); } //For ECC calculation
VOID KillControllers()
{
	XInputdPowerDownDevice(0x10000000);
	XInputdPowerDownDevice(0x10000001);
	XInputdPowerDownDevice(0x10000002);
	XInputdPowerDownDevice(0x10000003);
}

bool CheckPage(BYTE* page)
{
	unsigned int* data = (unsigned int*)page;
	unsigned int i=0, val=0, v=0;
	unsigned char edc[4];	
	for (i = 0; i < 0x1066; i++)
	{
		if (!(i & 31))
			v = ~bswap32(*data++);		
		val ^= v & 1;
		v>>=1;
		if (val & 1)
			val ^= 0x6954559;
		val >>= 1;
	}

	val = ~val;
	// 26 bit ecc data
	edc[0] = ((val << 6) | (page[0x20C] & 0x3F)) & 0xFF;
	edc[1] = (val >> 2) & 0xFF;
	edc[2] = (val >> 10) & 0xFF;
	edc[3] = (val >> 18) & 0xFF;
	return ((edc[0] == page[0x20C]) && (edc[1] == page[0x20D]) && (edc[2] == page[0x20E]) && (edc[3] == page[0x20F]));
}

int HasSpare(char* filename)
{
	BYTE buf[0x630];
	FILE* fd;
	printf("Checking %s for spare data... ", filename);
	if (fopen_s(&fd, filename, "rb") != 0)
	{		
		printf("Unable to open %s for reading, error: 0x%X\n", filename);
		return -1;
	}
	if (fread_s(buf, 0x630, 0x630, 1, fd) != 1)
	{
		printf("Unable to read 0x630 bytes from %s!\n", filename);
		fclose(fd);
		return -1;
	}
	fclose(fd);
	if (buf[0] != 0xFF && buf[1] != 0x4F)
	{
		printf("%s Doesn't contain the magic bytes expected for an Xbox 360 NAND image, aborting!\n", filename);
		return -1;
	}	
	for (int offset = 0; offset < 0x630; offset += 0x210)
	{
		if (CheckPage(&buf[offset]))
		{
			printf("Spare data detected!\n", filename);
			return 0;
		}
	}
	printf("No spare data detected!\n", filename);
	return 1;
}


void flasher()
{
	bool finished = false;
	started = true;
	KillControllers();
	time(&start);
	int tmp = HasSpare("game:\\updflash.bin");
	if (tmp == -1)
		XLaunchNewImage(XLAUNCH_KEYWORD_DEFAULT_APP, 0);
	bool isEcced = (tmp == 0);	
	/*if (!MMC)
	{
		if (!isEcced)
		{
			UI.YN_ShowMessageBox(L"Warning!", L"You are about to flash an image that don't contain SPARE data to a machine that requires it! Would you still like to continue?");

			while(!finished)
			{
				if( UI.YN_MsgBoxShowing )
				{
					if( XHasOverlappedIoCompleted( &UI.YN_Overlapped ) )
					{
						UI.YN_MsgBoxShowing = FALSE;
						DWORD dwResult = XGetOverlappedResult( &UI.YN_Overlapped, NULL, TRUE );
						if( dwResult == ERROR_SUCCESS )
						{
							switch((int)UI.YN_Result.dwButtonPressed)
							{
							case(1):
								finished = true;
								break;
							case(0):
								printf("Continue\n");
								finished = true;
								break;
							default:
								printf("Unexpected Result\n");
							}
						}
						else if( dwResult == ERROR_CANCELLED )
						{
							swprintf_s( UI.YN_Msg, L"MessageBox was cancelled by user.\n" );
						}
						else
						{
							swprintf_s( UI.YN_Msg, L"MessageBox completed with failure 0x%08x.\n", dwResult );
						}

					}
				}
			}
			
		}
		unsigned int r = sfcx_init();
		sfcx_printinfo(r);
		try_rawflash("game:\\updflash.bin");
		sfcx_setconf(config);
	}
	else
	{
		if (isEcced)
		{
			if (!AutoMode)
			{
				dprintf(MSG_WARNING_YOU_ARE_ABOUT_TO_FLASH_SPARE_TO_NO_SPARE_CONSOLE);
				for(;;)
				{
					m_pGamepad = ATG::Input::GetMergedInput();
					if (m_pGamepad->wPressedButtons & XINPUT_GAMEPAD_START)
						break;
					else if (m_pGamepad->wPressedButtons)
						XLaunchNewImage(XLAUNCH_KEYWORD_DEFAULT_APP, 0);
				}
			}
			else
			{
				dprintf(MSG_WARNING_YOU_ARE_ABOUT_TO_FLASH_SPARE_TO_NO_SPARE_CONSOLE_AUTO);
				AutoCountdown();
			}
		}
		try_rawflash4g("game:\\updflash.bin");
	}
	time(&end);
	tdiff = difftime(end,start);
	dprintf(MSG_COMPLETED_AFTER_SECONDS, tdiff);
	dprintf(MSG_REBOOTING_IN);
	for (int i = 5; i > 0; i--)
	{
		dprintf("%i", i);
		for (int j = 0; j < 4; j++)
		{
			Sleep(250);
			dprintf(".");
		}
	}
	dprintf(MSG_BYE);*/
	HalReturnToFirmware(HalKdRebootRoutine);
}



void dumper(char *filename, bool useSystem)
{	
	started = true;
	bool finished = false;
	if (!MMC)
	{
		
		sfcx_init();
		sfcx_setconf(config);
		int size = sfc.size_bytes_phys;
		if((size == (RAW_NAND_64*4)) || (size == (RAW_NAND_64*8)))
		{	
			if(useSystem)
			{
				size = RAW_NAND_64;
			}
		}
		//time(&start);
		unsigned int r = sfcx_init();
		printf("size before try_rawdump %X\n", size);
		ReadNand* rnand = new ReadNand(size, filename);
		//try_rawdump(filename, size);
		sfcx_setconf(config);
	}
	else
	{
		//time(&start);
		//CXuiTextElement CurrentNandProcess;
		ReadCorona4G* read4G = new ReadCorona4G(filename);
		//try_rawdump4g(filename);
	}
	//time(&end);
	//tdiff = difftime(end,start);
	printf("Completed after %i\n", tdiff);
	dumped = true;
}

/*void PrintExecutingCountdown(int max)
{
	for (; max > 0; max--)
	{
		dprintf(MSG_EXECUTING_COMMAND_IN_SECONDS, max);
		Sleep(1000);
	}
	dprintf(MSG_EXECUTING_COMMAND);
}*/


void Nand::WriteNand()
{
	//UITools UI;
	/*bool finished = false;
	MMC = (sfcx_detecttype() == 1); // 1 = MMC, 0 = RAW NAND
	if (!MMC)
	{
		config = sfcx_getconf();
	}

	UI.YN_ShowMessageBox(L"Make backup?", L"Would you like to make a backup of the nand before flashing?");

	while(!finished)
	{
		if( UI.YN_MsgBoxShowing )
		{
			if( XHasOverlappedIoCompleted( &UI.YN_Overlapped ) )
			{
				UI.YN_MsgBoxShowing = FALSE;
				DWORD dwResult = XGetOverlappedResult( &UI.YN_Overlapped, NULL, TRUE );
				if( dwResult == ERROR_SUCCESS )
				{
					switch((int)UI.YN_Result.dwButtonPressed)
					{
					case(1):
						printf("Do not make nand backup\n");
						flasher();
						finished = true;
						break;
					case(0):
						printf("Make nand backup\n");
						//dumper("game:\\recovery.bin");
						flasher();
						finished = true;
						break;
					default:
						printf("Unexpected Result\n");
					}
				}
				else if( dwResult == ERROR_CANCELLED )
				{
					swprintf_s( UI.YN_Msg, L"MessageBox was cancelled by user.\n" );
				}
				else
				{
					swprintf_s( UI.YN_Msg, L"MessageBox completed with failure 0x%08x.\n", dwResult );
				}

			}
		}
	}
	
	if (!MMC)
	{
		sfcx_setconf(config);
	}*/
}

void Nand::ReadNand(bool isSystemDump)
{
	MMC = (sfcx_detecttype() == 1); // 1 = MMC, 0 = RAW NAND
	if (!MMC)
	{
		config = sfcx_getconf();
	}
	dumper("game:\\flashdmp.bin", isSystemDump);
	
	if (!MMC)
	{
		sfcx_setconf(config);
	}
}