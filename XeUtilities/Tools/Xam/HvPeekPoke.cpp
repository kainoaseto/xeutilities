#include "stdafx.h"
#include "HvPeekPoke.h"
#include "Tools\General\Tools.h"

#pragma warning(push)
#pragma warning(disable:4826) // Get rid of the sign-extended warning

#define HvxCall QWORD _declspec(naked)
static HvxCall HvxExpansionInstall(DWORD PhysicalAddress, DWORD CodeSize) {
	__asm {
		li			r0, 0x70
		sc
		blr
	}
}
static HvxCall HvxExpansionCall(DWORD ExpansionId, QWORD Param1 = 0, QWORD Param2 = 0, QWORD Param3 = 0, QWORD Param4 = 0) {
	__asm {
		li			r0, 0x71
		sc
		blr
	}
}
	
HRESULT InitializeHvPeekPoke() {

	// Allocate physcial memory for this expansion
	VOID* pPhysExp = XPhysicalAlloc(0x1000, MAXULONG_PTR, 0, PAGE_READWRITE);
	DWORD physExpAdd = (DWORD)MmGetPhysicalAddress(pPhysExp);

	// Copy over our expansion data
	ZeroMemory(pPhysExp, 0x1000);
	memcpy(pPhysExp, HvPeekPokeExp, sizeof(HvPeekPokeExp));

	// Now we can install our expansion
	HRESULT result = (HRESULT)HvxExpansionInstall(physExpAdd, 0x1000);

	// Free our allocated data
	XPhysicalFree(pPhysExp);

	// Return our install result
	return result;
}
	
BYTE    HvPeekBYTE(QWORD Address) {
	return (BYTE)HvxExpansionCall(HvPeekPokeExpID, PEEK_BYTE, Address);
}
WORD    HvPeekWORD(QWORD Address) {
	return (WORD)HvxExpansionCall(HvPeekPokeExpID, PEEK_WORD, Address);
}
DWORD   HvPeekDWORD(QWORD Address) {
	return (DWORD)HvxExpansionCall(HvPeekPokeExpID, PEEK_DWORD, Address);
}
QWORD   HvPeekQWORD(QWORD Address) {
	return HvxExpansionCall(HvPeekPokeExpID, PEEK_QWORD, Address);
}
	
HRESULT HvPeekBytes(QWORD Address, PVOID Buffer, DWORD Size) {	
	
	// Create a physical buffer to peek into
	VOID* data = XPhysicalAlloc(Size, MAXULONG_PTR, 0, PAGE_READWRITE);
	ZeroMemory(data, Size);
	
	HRESULT result = (HRESULT)HvxExpansionCall(HvPeekPokeExpID, 
		PEEK_BYTES, Address, (QWORD)MmGetPhysicalAddress(data), Size);

	// If its successful copy it back
	if(result == S_OK) memcpy(Buffer, data, Size);

	// Free our physical data and return our result
	XPhysicalFree(data);
	return result;
}

HRESULT HvPokeBYTE(QWORD Address, BYTE Value) {
	return (HRESULT)HvxExpansionCall(HvPeekPokeExpID, POKE_BYTE, Address, Value);
}
HRESULT HvPokeWORD(QWORD Address, WORD Value) {
	return (HRESULT)HvxExpansionCall(HvPeekPokeExpID, POKE_WORD, Address, Value);
}
HRESULT HvPokeDWORD(QWORD Address, DWORD Value) {
	return (HRESULT)HvxExpansionCall(HvPeekPokeExpID, POKE_DWORD, Address, Value);
}
HRESULT HvPokeQWORD(QWORD Address, QWORD Value) {
	return (HRESULT)HvxExpansionCall(HvPeekPokeExpID, POKE_QWORD, Address, Value);
}
HRESULT HvPokeBytes(QWORD Address, const void* Buffer, DWORD Size) {

	// Create a physical buffer to poke from
	VOID* data = XPhysicalAlloc(Size, MAXULONG_PTR, 0, PAGE_READWRITE);
	memcpy(data, Buffer, Size);
	
	HRESULT result = (HRESULT)HvxExpansionCall(HvPeekPokeExpID, 
		POKE_BYTES, Address, (QWORD)MmGetPhysicalAddress(data), Size);

	// Free our physical data and return our result
	XPhysicalFree(data);
	return result;
}

wstring GetCpuKey()
{
	MemoryBuffer mb;
	BYTE cpuKey[0x10];
	BYTE cpukeyDigest[0x14];
	BYTE realCpuKey[0x10];

	if(mb.CheckSize(0x10))
	{
		HvPeekBytes(0x20, mb.GetData(), 0x10);
		memcpy(cpuKey, mb.GetData(), 0x10);
		XeCryptSha(cpuKey, 0x10, NULL, NULL, NULL, NULL, cpukeyDigest, XECRYPT_SHA_DIGEST_SIZE);
		HvPeekBytes(0x20, realCpuKey, 0x10);
		/*printf("CPUKEY: ");
		for(int i = 0; i < 0x10; i++)
			printf("%02X", realCpuKey[i]);
		printf("\n");*/

		char cKey[0x20];

		CharStr2HexStr(realCpuKey, cKey, 0x10);
		string sKey(cKey);
		wstring wKey = strtowstr(sKey);
		wKey.resize(32);
		return wKey;
	}
	return L"Error in GetCpukey";
}

wstring GetConsoleType()
{
	BOOL IsDevkit =  *(DWORD*)0x8E038610 & 0x8000 ? FALSE : TRUE;
	if(IsDevkit)
	{
		return L"DevKit";
	}
	else
	{
		return L"Retail";
	}
}
wstring GetMotherBoard()
{
	DWORD cType = CONSOLE_TYPE_FROM_FLAGS;
	switch(cType)
	{
	case 0x00000000:
		return L"Xenon";
	case 0x10000000: 
		return L"Zephyr";
	case 0x20000000: 
		return L"Falcon";
	case 0x30000000: 
		return L"Jasper";
	case 0x40000000:
		return L"Trinity";
	case 0x50000000: 
		return L"Corona";
	case 0x60000000:
		return L"Windchester";
	default:
		return L"Unknown";
	}

}

#pragma warning(pop)