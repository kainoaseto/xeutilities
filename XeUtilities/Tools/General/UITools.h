#pragma once
#include "stdafx.h"
#include "Tools\General\types.h"

class UITools : CXuiSceneImpl
{

public:

	static UITools& getInstance()
	{
		static UITools singleton;
		return singleton;
	}

	// Yes or No Message box variables
	LPCWSTR* YN_Buttons;
	MESSAGEBOX_RESULT YN_Result;
	BOOL YN_MsgBoxShowing;
	WCHAR YN_Msg[256];
	XOVERLAPPED YN_Overlapped;

	void YN_ShowMessageBox(LPCWSTR title, LPCWSTR text);

	// how to use: XNotifyQueueUICustom(L"message");
	HRESULT XNotifyQueueUICustom( WCHAR * buffer );

	void SetNandProgress(int val);
	void SetNandText(LPCWSTR text);

protected:
	UITools();
	~UITools();
	UITools(const UITools&);
	UITools operator=(const UITools&);

private:

	CXuiTextElement CurrentNandProcess;
	CXuiProgressBar NandProgressBar;

	void InitializeChildren();

	typedef	void (*XNotifyQueueUI)(u64 stringId, u64 playerIndex, u64 r5, WCHAR* displayText, u64 r7);

	FARPROC ResolveFunction(char* ModuleName, UINT32 Ordinal); 

}; 