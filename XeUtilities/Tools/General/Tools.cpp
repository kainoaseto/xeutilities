#include "stdafx.h"
#include "Tools.h"
wstring wstrappend(wstring wstr, int num)
{
	wstringstream wss;
	wss << num;
	wstr.append( wss.str() );
	return wstr;
}

wstring wstrappend(wstring wstr, wstring num)
{
	return wstr + num;
}

std::string int_to_hex( int i )
{
  std::stringstream stream;
  stream << std::uppercase
         /*<< std::setfill ('0') << std::setw(sizeof(int)*2) */
         << std::hex << i;
  return stream.str();
}

wstring strtowstr(string str)
{
	WCHAR* retVal;
	int Length = MultiByteToWideChar(CP_UTF8,0,str.c_str(),-1,NULL,0);
	retVal = new WCHAR[Length+1];
	MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, retVal, Length);

	//std::wstring ret(retVal);
	//delete[] retVal;
	
	return retVal;
}

string wstrtostr(wstring wstr)
{
	string s(wstr.begin(), wstr.end());
	s.assign(wstr.begin(), wstr.end());
	return s;
}

string sprintfa(const char *format, ...)
{
	char temp[16384];

	va_list ap;
	va_start (ap, format);
	vsprintf_s (temp, 16384, format, ap);
	va_end (ap);

	return temp;
}

int chartoint(const char* value)
{
	int num = 0;
	stringstream ss;
	ss << value;
	ss >> num;
	return num;
}

LPCSTR Char2LPCSTR(char* yourchar, LPCSTR addmsg, bool atstart)
{
	if(addmsg == NULL)														// If there are no other messages to be added on then just convert
	{
		std::string newstr = std::string(yourchar);							// Conversion of the char to a string
		LPCSTR newchar = newstr.c_str();									// Then into a LPCSTR
		return _strdup(newchar);											// _strdup stores the value of .c_str() since it is destroyed regulerly
	}

	if(atstart != true)														// If at start if anything but true then add the text to the end
	{
		std::string newstr = std::string(yourchar) + addmsg;				// Add the text to the end of the newly converted string
		LPCSTR newchar = newstr.c_str();
		return _strdup(newchar);
	}

	if(atstart)																// If atstart is true then it addes the message to the start of the string
	{
		std::string newstr = addmsg + std::string(yourchar);
		LPCSTR newchar = newstr.c_str();
		return _strdup(newchar);
	}
	else
	{
		printf("Error in C2STR", "Error");                                 // If none of those thigns happened there was a error
		return "Error in C2STR";
	}

}

string bytesToHex(char* data, const int datasize)
{
	ostringstream result;
	for(int i = 0; i < datasize; ++i)
	{
		result << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int)(unsigned char)data[i];
	}
	return result.str();
} 

//Function to convert unsigned char to string of length 2
void Char2Hex(unsigned char ch, char* szHex)
{
	unsigned char byte[2];
	byte[0] = ch/16;
	byte[1] = ch%16;
	for(int i=0; i<2; i++)
	{
		if(byte[i] >= 0 && byte[i] <= 9)
			szHex[i] = '0' + byte[i];
		else
			szHex[i] = 'A' + byte[i] - 10;
	}
	szHex[2] = 0;
}

//Function to convert string of length 2 to unsigned char
void Hex2Char(char const* szHex, unsigned char& rch)
{
	rch = 0;
	for(int i=0; i<2; i++)
	{
		if(*(szHex + i) >='0' && *(szHex + i) <= '9')
			rch = (rch << 4) + (*(szHex + i) - '0');
		else if(*(szHex + i) >='A' && *(szHex + i) <= 'F')
			rch = (rch << 4) + (*(szHex + i) - 'A' + 10);
		else
			break;
	}
}    

//Function to convert string of unsigned chars to string of chars
void CharStr2HexStr(unsigned char const* pucCharStr, char* pszHexStr, int iSize)
{
	int i;
	char szHex[3];
	pszHexStr[0] = 0;
	for(i=0; i<iSize; i++)
	{
		Char2Hex(pucCharStr[i], szHex);
		strcat(pszHexStr, szHex);
	}
}

//Function to convert string of chars to string of unsigned chars
void HexStr2CharStr(char const* pszHexStr, unsigned char* pucCharStr, int iSize)
{
	int i;
	unsigned char ch;
	for(i=0; i<iSize; i++)
	{
		Hex2Char(pszHexStr+2*i, ch);
		pucCharStr[i] = ch;
	}
}

HRESULT GetBytesString(BYTE* Data, UINT DataLen, CHAR* OutBuffer, UINT* OutLen) {

	// Check our lenghts
	if(*OutLen < (DataLen * 2))
		return S_FALSE;

	*OutLen = DataLen * 2;

	// Output into our buffer as hex
	CHAR hexChars[] = "0123456789ABCDEF";
	for(UINT x = 0, y = 0; x < DataLen; x++, y+=2) {
		OutBuffer[y] = hexChars[(Data[x] >> 4)];
		OutBuffer[y + 1] = hexChars[(Data[x] & 0x0F)];
	}

	// All done =)
	return S_OK;
}
