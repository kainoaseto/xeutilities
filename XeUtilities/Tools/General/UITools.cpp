#include "stdafx.h"
#include "UITools.h"

UITools::UITools()
{
	YN_Buttons = new LPCWSTR[2]();
	YN_Buttons[0] = L"Yes";
	YN_Buttons[1] = L"No";
}

UITools::~UITools() {}

void UITools::YN_ShowMessageBox(LPCWSTR title, LPCWSTR text)
{
	ATG::SignIn::Initialize( 0, 1, FALSE, 1 );
	ATG::SignIn::Update();
	DWORD dwRet;
	ZeroMemory( &YN_Overlapped, sizeof( XOVERLAPPED ) );
	printf("User: %d signed in\n", ATG::SignIn::GetSignedInUser());
	dwRet = XShowMessageBoxUI(XUSER_INDEX_ANY, title, text, (DWORD)2, YN_Buttons, 1, XMB_ALERTICON, &YN_Result, &YN_Overlapped);
	ASSERT(dwRet == ERROR_IO_PENDING);
	YN_MsgBoxShowing = TRUE;
	YN_Msg[0] = L'\0';
}

FARPROC UITools::ResolveFunction(char* ModuleName, UINT32 Ordinal) 
{	 
	HMODULE mHandle = GetModuleHandle(ModuleName);
	if(mHandle == NULL)
		return NULL;

	return GetProcAddress(mHandle, (LPCSTR)Ordinal);
}

HRESULT UITools::XNotifyQueueUICustom( WCHAR * buffer )
{
	// Notify a message on screen
	XNotifyQueueUI xNotifyQueueUI = (XNotifyQueueUI)ResolveFunction("xam.xex", 656);

	if(xNotifyQueueUI == NULL)
	{
		return S_FALSE;
	}
	xNotifyQueueUI(0x22, 0xFF, 1, buffer, 0);

	return S_OK;
}

void UITools::SetNandProgress(int val)
{
	NandProgressBar.SetValue(val);
}

void UITools::SetNandText(LPCWSTR text)
{
	CurrentNandProcess.SetText(text);
}

void UITools::InitializeChildren()
{
	GetChildById(L"CurrentNandProcess", &CurrentNandProcess);
	GetChildById(L"NandProgress", &NandProgressBar);
}

