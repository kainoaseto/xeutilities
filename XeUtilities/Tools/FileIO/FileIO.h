#pragma once
#include "stdafx.h"

HRESULT Mount(PCHAR drive, PCHAR device);
DWORD ReadFileToBuf(PCHAR path, BYTE* buffer, DWORD maxLen);
BOOL FileExists(PCHAR path);
void WriteFileToFlash(char* curPath, PCHAR fileName);
BOOL CWriteFile(const char* path, const VOID* Data, DWORD size);
