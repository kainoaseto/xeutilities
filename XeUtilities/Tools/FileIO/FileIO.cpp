#include "stdafx.h"
#include "FileIO.h"

HRESULT Mount(PCHAR drive, PCHAR device)
{
	CHAR destinationDrive[MAX_PATH];
	sprintf_s(destinationDrive, MAX_PATH, "\\??\\%s", drive);
	STRING s_Device = MAKE_STRING(device);
	STRING s_Link = MAKE_STRING(destinationDrive);
	ObDeleteSymbolicLink(&s_Link);
	return (HRESULT)ObCreateSymbolicLink(&s_Link, &s_Device);
}

BOOL FileExists(PCHAR path)
{
	HANDLE fHandle = CreateFile(path, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if(fHandle == INVALID_HANDLE_VALUE)
		return 0;
	CloseHandle(fHandle);
	return 1;
}

DWORD ReadFileToBuf(PCHAR path, BYTE* buffer, DWORD maxLen)
{
	DWORD retBuf = 0;
	HANDLE fHandle = CreateFileA(path, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if(fHandle != INVALID_HANDLE_VALUE)
	{
		ZeroMemory(buffer, maxLen);
		ReadFile(fHandle, buffer, maxLen, &retBuf, NULL);
		CloseHandle(fHandle);
	}
	return retBuf;
}

BOOL CWriteFile(const char* path, const VOID* data, DWORD size)
{
	HANDLE fHandle = CreateFile(path, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if(fHandle == INVALID_HANDLE_VALUE)
	{
		printf("CWriteFile - failed to create handle");
		return FALSE;
	}
	DWORD writeSize = size;
	if(WriteFile(fHandle, data, writeSize, &writeSize, NULL) != TRUE)
	{
		printf("CWriteFile - failed to write file");
		return FALSE;
	}
	CloseHandle(fHandle);

	return TRUE;

}

void WriteFileToFlash(char* curPath, PCHAR fileName)
{
	char pFileName[MAX_PATH] = "Flash:\\";
	strcat_s(pFileName, MAX_PATH, fileName);
	printf("path to write file: %s\n", pFileName);
	HANDLE fHandle = CreateFile(pFileName, GENERIC_WRITE|SYNCHRONIZE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if(fHandle == INVALID_HANDLE_VALUE)
	{
		printf("WriteFileToFlash - fHandle failed to create\n");
		return;
	}
	BYTE* data = new BYTE[0x10000]; // 65,536 bytes ~ 64kb max file size to write to nand for now
	DWORD length;
	length = ReadFileToBuf(curPath, data, 0x10000);
	DWORD written = 0;
	DWORD nlen = 0; // This is jsut to hold the written amount since ofr some reason WriteFile needs a var there
	printf("Data: 0x%x\n", data);
	printf("&length: 0x%x\n", &length);
	printf("length: 0x%x\n", length);
	
	while(length >= 0x4000)
	{
		WriteFile(fHandle, (data+written), 0x4000, &nlen, NULL);
		length -= 0x4000;
		written += 0x4000;
		printf("%i: wrote 0x%x bytes, 0x%x remaining\n", written/0x4000, written, length);
	}

	if(length != 0)
	{
		WriteFile(fHandle, data+written, length, &nlen, NULL);
		written += length;
		printf("wrote: 0x%x bytes\n", written);
	}
	CloseHandle(fHandle);
	printf("wrote file to flash\n");

}