#pragma once

#include "Tools\FileIO\FileIO.h"
#include "Tools\General\Tools.h"

class Settings
{
public:
	static Settings& getInstance()
	{
		static Settings singleton;
		return singleton;
	}

	int GetHorizOverscan() { return m_HorizOverscan; }
	void SetHorizOverscan(int value);

	int GetVertOverscan() {	return m_VertOverscan; }
	void SetVertOverscan(int value);

	int GetEnableLetterbox() { return m_EnableLetterbox; }
	void SetEnableLetterbox(int value);

	void CreateIniSettings();
	void GetIniSettings();

	const char* GetIniValue(char* section, char* key);

	void UpdateIniSetting(const char* section, const char* Setting, const char* value);

	int GetFanSpeed() { return m_FanSpeed; }
	void SetFanSpeed(int value);

	int GetCpuThreshhold() { return m_CpuThreshhold; }
	void SetCpuThreshhold(int value);

	int GetGpuThreshhold() { return m_GpuThreshhold; }
	void SetGpuThreshhold(int value);

	int GetMemThreshhold() { return m_MemThreshhold; }
	void SetMemThreshhold(int value);

private:
	int m_HorizOverscan;
	int m_VertOverscan;
	int m_EnableLetterbox;

	int m_FanSpeed;
	int m_CpuThreshhold;
	int m_GpuThreshhold;
	int m_MemThreshhold;

	void DefaultSettings();

	Settings();
	~Settings() {}
	Settings(const Settings&);
	Settings operator=(const Settings&);
};