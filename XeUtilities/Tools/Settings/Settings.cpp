#include "stdafx.h"
#include "Settings.h"
#include "Tools\IniManager\SimpleIni.h"

Settings::Settings()
{
	DefaultSettings();
	if(!FileExists("game:\\settings.ini"))
	{
		printf("Created new settings.ini");
		CreateIniSettings();
	}

	GetIniSettings();
}

void Settings::DefaultSettings()
{
	m_HorizOverscan = 0;
	m_VertOverscan = 0;
	m_EnableLetterbox = 1;
}

void Settings::CreateIniSettings()
{
	CSimpleIniA IniData(1, 0, 0);
	IniData.SetValue("SMC", "fanspeed", "auto");
	IniData.SetValue("SMC", "cputarget", "70");
	IniData.SetValue("SMC", "gputarget", "70");
	IniData.SetValue("SMC", "memtarget", "70");
	IniData.SetValue("SMC", "toprightROL", "green");
	IniData.SetValue("SMC", "topleftROL", "green");
	IniData.SetValue("SMC", "bottomleftROL", "green");
	IniData.SetValue("SMC", "bottomrightROL", "green");
	IniData.SetValue("Settings", "vertoverscan", "0");
	IniData.SetValue("Settings", "horizoverscan", "0");
	IniData.SetValue("Settings", "enableletterbox", "1");
	IniData.SaveFile("game:\\settings.ini");
}

void Settings::UpdateIniSetting(const char* section, const char* setting, const char* value)
{
	CSimpleIniA IniData(1, 0, 0);
	SI_Error SE = IniData.LoadFile("game:\\settings.ini");
	if(SE >= 0)
	{
		IniData.SetValue(section, setting, value);
		IniData.SaveFile("game:\\settings.ini");
	}

}

const char* Settings::GetIniValue(char* section, char* key)
{
	CSimpleIniA IniData(1, 0, 0);
	SI_Error SE = IniData.LoadFile("game:\\settings.ini");
	if(SE >= 0)
	{
		const char* value = IniData.GetValue(section, key);
		printf("Loaded %s from INI: %s\n", key, value);
		return value;
	}
	return "Failed to get value";
	
}

void Settings::GetIniSettings()
{
	CSimpleIniA IniData(1, 0, 0);
	SI_Error SE = IniData.LoadFile("game:\\settings.ini");
	if(SE >= 0)
	{
		if(IniData.GetValue("SMC", "fanspeed") == "auto")
		{
			m_FanSpeed = 25;
		}
		else
		{
			m_FanSpeed = chartoint(IniData.GetValue("SMC", "fanspeed"));
		}
		m_CpuThreshhold = chartoint(IniData.GetValue("SMC", "cputarget"));
		m_GpuThreshhold = chartoint(IniData.GetValue("SMC", "gputarget"));
		m_MemThreshhold = chartoint(IniData.GetValue("SMC", "memtarget"));
		m_EnableLetterbox = chartoint(IniData.GetValue("Settings", "enableletterbox"));
		m_HorizOverscan = chartoint(IniData.GetValue("Settings", "horizoverscan"));
		m_VertOverscan = chartoint(IniData.GetValue("Settings", "vertoverscan"));
		printf("Loaded settings from INI: %d, %d, %d, %d\n", m_FanSpeed, m_CpuThreshhold, m_GpuThreshhold, m_MemThreshhold);
	}
}

void Settings::SetFanSpeed(int value)
{
	m_FanSpeed = value;
	if(value <= 25)
	{
		UpdateIniSetting("SMC", "fanspeed", "auto");
	}

	else
	{
		char s_value[256];
		sprintf(s_value, "%d", value);

		UpdateIniSetting("SMC", "fanspeed", s_value);
	}
}

void Settings::SetCpuThreshhold(int value)
{
	m_CpuThreshhold = value;
	char s_value[256];
	sprintf(s_value, "%d", value);

	UpdateIniSetting("SMC", "cputarget", s_value);
}

void Settings::SetGpuThreshhold(int value)
{
	m_GpuThreshhold = value;
	char s_value[256];
	sprintf(s_value, "%d", value);

	UpdateIniSetting("SMC", "gputarget", s_value);
}

void Settings::SetMemThreshhold(int value)
{
	m_MemThreshhold = value;
	char s_value[256];
	sprintf(s_value, "%d", value);

	UpdateIniSetting("SMC", "memtarget", s_value);
}

void Settings::SetHorizOverscan(int value)
{
	m_HorizOverscan = value;
	char s_value[256];
	sprintf(s_value, "%d", value);

	UpdateIniSetting("Settings", "horizoverscan", s_value);
}

void Settings::SetVertOverscan(int value)
{
	m_VertOverscan = value;
	char s_value[256];
	sprintf(s_value, "%d", value);

	UpdateIniSetting("Settings", "vertoverscan", s_value);
}

void Settings::SetEnableLetterbox(int value)
{
	m_EnableLetterbox = value;
	char s_value[256];
	sprintf(s_value, "%d", value);

	UpdateIniSetting("Settings", "enableletterbox", s_value);
}