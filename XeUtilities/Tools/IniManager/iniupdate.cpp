#include "stdafx.h"

/*#include <xtl.h>
#include "Tools\Kernel\kernel.h"
#include "util.h"
#include "SimpleIni.h"

#define INI_NAME		"ulaunch:\\launch.ini"
#define FLASH_INI_NAME	"GAME:\\files\\launch.ini"
#define MOUNT_NAME		"ulaunch:"
#define BUTTON_KEY		"QuickLaunchButtons"
#define PLUGIN_KEY		"Plugins"
#define SETTING_KEY		"Settings"

#define TOTAL_PATHS		12
#define INI_PATHS		6
#define TOTAL_BUTTONS	8
#define MOUNT_FLASH		5 // gonna need this for flash updating...

char* butNames[] = {
	"Default",
	"BUT_A",
	"BUT_B",
	"BUT_X",
	"BUT_Y",
	"Start",
	"Back",
	"LBump"
};

// supported mount paths
const char* mountPath[] = {
	"\\Device\\Mass0\\",
	"\\Device\\Mass1\\",
	"\\Device\\Mass2\\",
	"\\Device\\Harddisk0\\Partition1\\",
	"\\Device\\BuiltInMuUsb\\Storage\\", // flash memory unit big block
	"\\Device\\Flash\\",
	"\\Device\\Mu0\\",
	"\\Device\\Mu1\\",
	"\\Device\\Mass0PartitionFile\\Storage\\",
	"\\Device\\Mass1PartitionFile\\Storage\\",
	"\\Device\\Mass2PartitionFile\\Storage\\",
	"\\Device\\Cdrom0\\",
};

const char* newNames[] = {
	"Usb:\\",
	"Usb:\\",
	"Usb:\\",
	"Hdd:\\",
	"FlashMu:\\",
	"Sfc:\\",
	"Mu:\\",
	"Mu:\\",
	"UsbMu:\\",
	"UsbMu:\\",
	"UsbMu:\\",
	"Dvd:\\",
};

char tempValue[MAX_PATH];

char* updateButton(const char* iniVal)
{
	int i;
	for(i = 0; i < TOTAL_PATHS; i++)
	{
			strcpy_s(tempValue, MAX_PATH, newNames[i]);
			return tempValue;
	}
	return NULL;
}

char* updatePlugin(const char* iniVal)
{
	int i;
	for(i = 0; i < TOTAL_PATHS; i++)
	{
		//example conversion
		//\Device\Harddisk0\Partition1\somedllxex.xex to Hdd:\somedllxex.xex
		if(strnicmp(iniVal, mountPath[i], strlen(mountPath[i])) == 0)
		{
			//DbgPrint("p:string match found for %s\n", mountPath[i]);
			strcpy_s(tempValue, MAX_PATH, newNames[i]);
			strcat_s(tempValue, MAX_PATH, &iniVal[strlen(mountPath[i])]);
			return tempValue;
		}
	}
	return NULL;
}

VOID loadAndUpdateIni(int mountnum)
{
	const char* iniValue;
	char* updRet;
	int j;
	BOOL hasUpdate = FALSE;
	CSimpleIniA iniData(1, 0, 0);
	SI_Error rc = iniData.LoadFile(INI_NAME);
	if (rc >= 0)
	{
		//DbgPrint("ini loaded OK\n");
		// scan it's paths
		for(j = 0; j < TOTAL_BUTTONS; j++) // check the buttons
		{
			iniValue = iniData.GetValue(BUTTON_KEY, butNames[j], NULL);
			if((iniValue != NULL)&&(iniValue[0] != 0))
			{
				//DbgPrint("attempting to update %s\n", iniValue);
				updRet = updateButton(iniValue);
				if(updRet != NULL)
				{
					//DbgPrint("update '%s' from '%s' to '%s'\n", butNames[j], iniValue, updRet);
					iniData.SetValue(BUTTON_KEY, butNames[j], updRet, NULL, true);
					hasUpdate = TRUE;
				}
			}
		}
		// check the plugins
		for(j = 0; j < 5; j++)
		{
			char cat[] = "plugin1";
			cat[6] = (j + 0x31) & 0xFF;
			iniValue = iniData.GetValue(PLUGIN_KEY, cat, NULL);
			if((iniValue != NULL)&&(iniValue[0] != 0))
			{
				//DbgPrint("attempting to update %s\n", iniValue);
				updRet = updatePlugin(iniValue);
				if(updRet != NULL)
				{
					//DbgPrint("update plugin%d from '%s' to '%s'\n", j, iniValue, updRet);
					iniData.SetValue(PLUGIN_KEY, cat, updRet, NULL, true);
					hasUpdate = TRUE;
				}
			}
		}
		// check settings
		dprintf("Would you like to change settings? Press A for yes or B for no\n");
		if(getConfirm())
		{
			BOOL bCurr, confirm;
			dprintf("Do you want to be able to relaunch from miniblade in NXE dash? A = yes B = no\n");
			bCurr = iniData.GetBoolValue(SETTING_KEY, "nxemini", TRUE);
			confirm = getConfirm();
			if(confirm != bCurr)
			{
				iniData.SetBoolValue(SETTING_KEY, "nxemini", (confirm!=0), NULL, TRUE);
				hasUpdate = TRUE;
			}

			bCurr = iniData.GetBoolValue(SETTING_KEY, "pingpatch", FALSE);
			dprintf("Do you want to enable FBDev's ping limit patch? A = yes B = no\n");
			confirm = getConfirm();
			if(confirm != bCurr)
			{
				iniData.SetBoolValue(SETTING_KEY, "pingpatch", (confirm!=0), NULL, TRUE);
				hasUpdate = TRUE;
			}

			bCurr = iniData.GetBoolValue(SETTING_KEY, "contpatch", FALSE);
			dprintf("Do you want to enable mojobojo's content unlock patch? A = yes B = no\n");
			confirm = getConfirm();
			if(confirm != bCurr)
			{
				iniData.SetBoolValue(SETTING_KEY, "contpatch", (confirm!=0), NULL, TRUE);
				hasUpdate = TRUE;
			}

			bCurr = iniData.GetBoolValue(SETTING_KEY, "fatalfreeze", FALSE);
			dprintf("Do you want the console to freeze on fatal errors? A = yes B = no\n");
			confirm = getConfirm();
			if(confirm != bCurr)
			{
				iniData.SetBoolValue(SETTING_KEY, "fatalfreeze", (confirm!=0), NULL, TRUE);
				hasUpdate = TRUE;
			}

			if(confirm == FALSE)
			{
				bCurr = iniData.GetBoolValue(SETTING_KEY, "fatalreboot", FALSE);
				dprintf("Do you want the console to reboot on fatal errors? A = yes(reboot) B = no(shutdown)\n");
				confirm = getConfirm();
				if(confirm != bCurr)
				{
					iniData.SetBoolValue(SETTING_KEY, "fatalreboot", (confirm!=0), NULL, TRUE);
					hasUpdate = TRUE;
				}
			}

			bCurr = iniData.GetBoolValue(SETTING_KEY, "regionspoof", FALSE);
			dprintf("Do you want to enable ability to region spoof with RB on game launch? A = yes B = no\n");
			confirm = getConfirm();
			if(confirm != bCurr)
			{
				iniData.SetBoolValue(SETTING_KEY, "regionspoof", (confirm!=0), NULL, TRUE);
				hasUpdate = TRUE;
			}

			bCurr = iniData.GetBoolValue(SETTING_KEY, "dvdexitdash", FALSE);
			dprintf("When a default item is set and you are playing a video, exit back to NXE? A = yes B = no\n");
			confirm = getConfirm();
			if(confirm != bCurr)
			{
				iniData.SetBoolValue(SETTING_KEY, "dvdexitdash", (confirm!=0), NULL, TRUE);
				hasUpdate = TRUE;
			}

			bCurr = iniData.GetBoolValue(SETTING_KEY, "xblaexitdash", FALSE);
			dprintf("When you are playing an XBLA game and use the exit option, exit to NXE? A = yes B = no\n");
			confirm = getConfirm();
			if(confirm != bCurr)
			{
				iniData.SetBoolValue(SETTING_KEY, "xblaexitdash", (confirm!=0), NULL, TRUE);
				hasUpdate = TRUE;
			}

			bCurr = iniData.GetBoolValue(SETTING_KEY, "nosysexit", FALSE);
			dprintf("Do you want to remove the ability to go to system settings via miniblades? A = yes B = no\n");
			confirm = getConfirm();
			if(confirm != bCurr)
			{
				iniData.SetBoolValue(SETTING_KEY, "nosysexit", (confirm!=0), NULL, TRUE);
				hasUpdate = TRUE;
			}

			bCurr = iniData.GetBoolValue(SETTING_KEY, "noupdater", TRUE);
			dprintf("Do you want block the xbox from finding system updates? A = yes B = no\n");
			confirm = getConfirm();
			if(confirm != bCurr)
			{
				iniData.SetBoolValue(SETTING_KEY, "nosysexit", (confirm!=0), NULL, TRUE);
				hasUpdate = TRUE;
			}
		}

		// save the ini file (all paths but flash ini file can be updated directly)
		if(hasUpdate)
		{
			SI_Error err;
			
			if(mountnum != MOUNT_FLASH)
			{
				dprintf("Writing file...");
				err = iniData.SaveFile(INI_NAME);
			}
			else
			{
				dprintf("Writing file for flash installation...");
				err = iniData.SaveFile(FLASH_INI_NAME); // save the file to be reflashed later
			}
			
			if(err == 0)
				dprintf("Success!\n");
			else
				dprintf("Failed! (error %x)\n", err);
		}
		else
			dprintf("File has no updates done\n");

		iniData.Reset();// free up all simpleini allocs
	}
}

VOID iniUpdate(VOID)
{
	int i;
	for(i = 0; i < INI_PATHS; i++)
	{
		if(NT_SUCCESS(Mount(MOUNT_NAME, (PCHAR)mountPath[i])))
		{
			if(fileExists(INI_NAME))
			{
				//DbgPrint("ini found at %s\n", mountPath[i]);
				dprintf("\nlaunch.ini found at %s\n", mountPath[i]);
				dprintf("Press A to update or B to skip\n", mountPath[i]);
				if(i == MOUNT_FLASH)
					dprintf("**note that updating flash ini will overwrite any existing launch.ini included with this xex**\n");
				// load the ini file and update it
				if(getConfirm())
					loadAndUpdateIni(i);
			}
		}
	}
}*/
