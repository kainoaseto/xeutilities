#include "stdafx.h"
#include "SimpleIni.h"
#include "Tools\FileIO\FileIO.h"

void LinkInternalDrives()
{
	// Create the INI file in the games root only temp
	CSimpleIniA IniData(1, 0, 0);
	IniData.SetValue("xbdm", "drivemap internal","1");
	IniData.SetSpaces(false);
	IniData.SaveFile("game:\\recint.ini");

	if(FileExists("Flash:\\recint.ini"))
	{
		DeleteFile("Flash:\\recint.ini");
		printf("Found recint.ini, deleting...\n");
	}
	// Write the file to the flash
	WriteFileToFlash("game:\\recint.ini", "recint.ini");

	// Delete Temp file
		
	DeleteFile("game:\\recint.ini");
}